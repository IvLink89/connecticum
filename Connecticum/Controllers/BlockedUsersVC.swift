//
//  BlockedUsersVC.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/20/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit
import ProgressHUD

class BlockedUsersVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UserCellDelegate{
    
    @IBOutlet weak var lblNotification: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var blockedUsersArray : [User] = []
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    //TODO delete from list in profile section
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "blocked_title".localized()
        navigationItem.backBarButtonItem?.title = ""
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        loadUsers()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        lblNotification.isHidden = blockedUsersArray.count != 0
        return blockedUsersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserCell
        cell.userCellDelegate = self
        cell.generateUserCell(user: blockedUsersArray[indexPath.row], indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "profile_unblock_btn".localized()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        var tempBlockedUsers = User.currentUser()!.blockedUsers
        let userIdToUnblock = blockedUsersArray[indexPath.row].objectId
        tempBlockedUsers.remove(at: tempBlockedUsers.firstIndex(of: userIdToUnblock)!)
        blockedUsersArray.remove(at: indexPath.row)
        updateCurrentUserInFirestore(withValues: [BLOCKEDUSERID : tempBlockedUsers]) { (error) in
            if error != nil {
                ProgressHUD.showError(error!.localizedDescription)
            }
            self.tableView.reloadData()
        }
    }
    
    func loadUsers() {
        if User.currentUser()!.blockedUsers.count > 0 {
            ProgressHUD.show()
            getUsersFromFirestore(withIds: User.currentUser()!.blockedUsers) { (allBlockedUsers) in
                ProgressHUD.dismiss()
                self.blockedUsersArray = allBlockedUsers
                self.tableView.reloadData()
            }
        }
    }
    
    func didClickAvatar(indexPath: IndexPath) {
        let profileVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        profileVC.user = blockedUsersArray[indexPath.row]
        self.navigationController?.pushViewController(profileVC, animated: true)
    }

}
