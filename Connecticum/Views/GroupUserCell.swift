//
//  GroupUserCell.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/21/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit

protocol GroupUsersDelegate {
    func didClickDelete(indexPath:IndexPath)
}

class GroupUserCell: UICollectionViewCell {
    
    @IBOutlet weak var ivAvatar: UIImageView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lblName: UILabel!
    var indexPath:IndexPath!
    var delegate:GroupUsersDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func generateCell(user:User, indexPath:IndexPath)  {
        self.indexPath = indexPath
        lblName.text = user.firstname
        if user.avatar != ""{
            imageFromData(pictureData: user.avatar) { (image) in
                if image != nil{
                    ivAvatar.image = image?.circleMasked
                }
            }
        }
    }
    
    @IBAction func onCloseClicked(_ sender: Any) {
        delegate!.didClickDelete(indexPath: indexPath)
    }
}
