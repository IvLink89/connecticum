//
//  MapVC.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/19/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit
import MapKit

class MapVC: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    var location: CLLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "map_title".localized()
        setupUI()
        createRightButton()
    }
    
    func setupUI() {
        navigationController?.navigationBar.backItem?.title = ""
        
        var region = MKCoordinateRegion()
        region.center.latitude = location.coordinate.latitude
        region.center.longitude = location.coordinate.longitude
        region.span.latitudeDelta = 0.01
        region.span.longitudeDelta = 0.01
        
        mapView.setRegion(region, animated: false)
        mapView.showsUserLocation = true
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location.coordinate
        mapView.addAnnotation(annotation)
    }
    
    func createRightButton() {
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(title: "map_btn_open".localized(), style: .plain, target: self, action: #selector(self.openInMap))]
    }
    
    @objc func openInMap() {
        let regionDestination: CLLocationDistance = 10000
        let coordinates = location.coordinate
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDestination, longitudinalMeters: regionDestination)
        
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan:  regionSpan.span)
        ]
        
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "map_user_location".localized()
        mapItem.openInMaps(launchOptions: options)
    }

}
