//
//  PhoneLoginVC.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/26/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit
import FirebaseAuth
import ProgressHUD

class PhoneLoginVC: UIViewController {

    @IBOutlet weak var btnAccept: AppRoundedButton!
    @IBOutlet weak var tfCode: AppTextField!
    @IBOutlet weak var tfMobile: AppTextField!
    @IBOutlet weak var tfCountryCode: AppTextField!
    
    var phoneNumber: String!
    var verificationId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfCountryCode.text = CountryCode().currentCode
    }

    @IBAction func onAcceptClicked(_ sender: Any) {
        if verificationId != nil{
            registerUser()
            return
        }
        if tfMobile.text !=  "" && tfCountryCode.text != ""{
            let fullNumber = tfCountryCode.text! + tfMobile.text!
            PhoneAuthProvider.provider().verifyPhoneNumber(fullNumber, uiDelegate: nil) { (id, error) in
                if error != nil {
                    ProgressHUD.showError(error?.localizedDescription)
                    return
                }
                self.verificationId = id
                self.updateUI()
            }
        }else{
            ProgressHUD.showError("Phone number is needed")
        }
    }
    
    func registerUser() {
        if verificationId != nil && tfCode.text != ""{
            User.registerUserWith(phoneNumber: phoneNumber, verificationCode: tfCode.text!, verificationId: verificationId) { (error, canLogin) in
                if error != nil {
                    ProgressHUD.showError(error?.localizedDescription)
                    return
                }
                if canLogin{
                    self.goToApp()
                }else{
                    self.performSegue(withIdentifier: "segToRegister", sender: self)
                }
            }
        }else{
            ProgressHUD.showError("Insert the code")
        }
    }
    
    func updateUI() {
        btnAccept.setTitle("Submit", for: .normal)
        phoneNumber = tfCountryCode.text! + tfMobile.text!
        tfCountryCode.isEnabled = false
        tfMobile.isEnabled = false
        tfMobile.placeholder = tfMobile.text
        tfMobile.text = ""
        tfCode.isHidden = false
        
    }
    
    func goToApp() {
        ProgressHUD.dismiss()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: USER_DID_LOGIN_NOTIFICATION), object: nil, userInfo: [USERID: User.currentId()])
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "MainVC"){
            self.presentDetail(controller)
        }
    }
}
