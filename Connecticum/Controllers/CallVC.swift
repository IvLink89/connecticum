//
//  CallVC.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/25/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit
import Sinch

class CallVC: UIViewController, SINCallDelegate {

    @IBOutlet weak var btnDecline: UIButton!
    @IBOutlet weak var btnEndCall: UIButton!
    @IBOutlet weak var btnAnswer: UIButton!
    @IBOutlet weak var btnSound: UIButton!
    @IBOutlet weak var btnMute: UIButton!
    @IBOutlet weak var ivAvatar: UIImageView!
    @IBOutlet weak var lblTime: AppMediumLabel!
    @IBOutlet weak var lblName: AppMediumLabel!
    
    var speaker = false
    var mute = false
    var durationTimer:Timer?
    var call:SINCall!
    var callAnswered = false
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    override func viewWillAppear(_ animated: Bool) {
        lblName.text = "Unknown"
        let id = call.remoteUserId
        getUsersFromFirestore(withIds: [id!]) { (users) in
            if users.count > 0{
                let user = users.first
                self.lblName.text = user?.fullname
                
                imageFromData(pictureData: (user?.avatar)!, withBlock: { (image) in
                    self.ivAvatar.image = image!.circleMasked
                })
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        call.delegate = self
        print("call.direction \(call.direction) - \(SINCallDirection.incoming)")
        if call.direction == SINCallDirection.incoming{
            showButtons()
            getAudioController().startPlayingSoundFile(pathToSound(soundName: "incoming"), loop: true)
        }else{
            callAnswered = true
            setCallStatus(text: "Calling...")
            showButtons()
        }
    }
    
    func callDidProgress(_ call: SINCall!) {
        setCallStatus(text: "Ringing...")
        getAudioController().startPlayingSoundFile(pathToSound(soundName: "ringback"), loop: true)
    }
    
    func callDidEstablish(_ call: SINCall!) {
        print("callDidEstablish!!!!!!")
        startCallDurationTimer()
        showButtons()
        getAudioController().stopPlayingSoundFile()
    }
    
    func callDidEnd(_ call: SINCall!) {
        getAudioController().stopPlayingSoundFile()
        stopDurationTimer()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onDeclineClicked(_ sender: Any) {
        call.hangup()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onEndCallClicked(_ sender: Any) {
        call.hangup()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onAnswerClicked(_ sender: Any) {
        callAnswered = true
        showButtons()
        getAudioController().stopPlayingSoundFile()
        call.answer()
    }
    
    @IBAction func onSoundClicked(_ sender: Any) {
        if speaker {
            speaker = false
            getAudioController().disableSpeaker()
            btnSound.setImage(UIImage(named: "speaker"), for: .normal)
        }else{
            speaker = true
            getAudioController().enableSpeaker()
            btnSound.setImage(UIImage(named: "speakerSelected"), for: .normal)
        }
    }
    
    @IBAction func onMuteClicked(_ sender: Any) {
        if mute {
            mute = false
            getAudioController().unmute()
            btnMute.setImage(UIImage(named: "mute"), for: .normal)
        }else{
            mute = true
            getAudioController().mute()
            btnMute.setImage(UIImage(named: "muteSelected"), for: .normal)
        }
    }
    
    func startCallDurationTimer(){
        print("startCallDurationTimer")
        durationTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(onDuration), userInfo: nil, repeats: true)
    }
    
    @objc func onDuration(){
        let duration = Date().timeIntervalSince(call.details.establishedTime)
        print("duration!!!!!! \(duration)")
        updateTimerLabel(seconds: Int(duration))
    }
    
    func updateTimerLabel(seconds:Int){
        let min = String(format: "%02d", (seconds / 60))
        let sec = String(format: "%02d", (seconds % 60))
        setCallStatus(text: "\(min) : \(sec)")
    }

    func stopDurationTimer() {
        if durationTimer != nil{
            durationTimer?.invalidate()
            durationTimer = nil
        }
    }
    
    func setCallStatus(text:String){
        lblTime.text = text
    }

    func showButtons() {
        if callAnswered{
            btnDecline.isHidden = true
            btnEndCall.isHidden = false
            btnAnswer.isHidden = true
            btnMute.isHidden = false
            btnSound.isHidden = false
        }else{
            btnDecline.isHidden = false
            btnEndCall.isHidden = true
            btnAnswer.isHidden = false
            btnMute.isHidden = true
            btnSound.isHidden = true
        }
    }
    
    func getAudioController() -> SINAudioController {
        return appDelegate.client.audioController()
    }
    
    func setCall(call:SINCall) {
        self.call = call
        call.delegate = self
    }
    
    func pathToSound(soundName:String) -> String {
        return Bundle.main.path(forResource: soundName, ofType: "wav")!
    }
}
