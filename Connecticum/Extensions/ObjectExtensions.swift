//
//  ObjectExtensions.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/2/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import Foundation

extension String {
    func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        return NSLocalizedString(self, tableName: tableName, value: "\(self)", comment: "")
    }
}
