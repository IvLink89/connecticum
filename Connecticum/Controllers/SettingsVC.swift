//
//  SettingsVC.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/8/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit
import Firebase
import ProgressHUD

class SettingsVC: UITableViewController {

    @IBOutlet weak var lblName: AppMediumLabel!
    @IBOutlet weak var ivAvatar: UIImageView!
    @IBOutlet weak var btnLogout: AppRoundedButton!
    @IBOutlet weak var btnDelete: AppButtonWithoutFrame!
    @IBOutlet weak var switchAvatars: UISwitch!
    @IBOutlet weak var lblShowAvatar: UILabel!
    
    var avatarSwitchStatus = false
    var firstLoad: Bool?
    
    override func viewDidAppear(_ animated: Bool) {
        if User.currentUser() != nil {
            setupUI()
            loadUserDefaults()
        }
    }
    
    override func viewDidLoad() {
        navigationItem.title = "settings_title".localized()
        lblShowAvatar?.text = "settings_show_avatar".localized()
//        tabBarController?.tabBar.backgroundImage = UIImage()
//        tabBarController?.tabBar.shadowImage = UIImage()
//        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    @IBAction func onEditClicked(_ sender: Any) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC"){
            self.presentDetail(controller)
        }
    }
    
    @IBAction func onLogoutClicked(_ sender: Any) {
        User.logOutCurrentUser { (isSuccess) in
            if isSuccess{
                self.makeTransitionToLogin()
            }
        }
    }
    
    @IBAction func onCleanCacheClicked(_ sender: Any) {
        do {
            let files = try FileManager.default.contentsOfDirectory(atPath: getDocumentsURL().path)
            for file in files {
                try FileManager.default.removeItem(atPath: "\(getDocumentsURL().path)/\(file)")
            }
            ProgressHUD.showSuccess("settings_cache_cleaned".localized())
        } catch {
            ProgressHUD.showError("settings_cache_didnt_clean".localized())
        }
    }
    
    @IBAction func onShareClicked(_ sender: Any) {
        let text = "settings_cache_didnt_clean".localized() + " \(APPURL)"
        let objectsToShare:[Any] = [text]
        let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.setValue("settings_cache_didnt_clean".localized(), forKey: "subject")
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func avatarsChangingSwitched(_ sender: UISwitch) {
        avatarSwitchStatus = sender.isOn
        saveUserDefaults()
    }
    
    @IBAction func onTermsClicked(_ sender: Any) {
    }
    
    @IBAction func onDeleteClicked(_ sender: Any) {
        let optionMenu = UIAlertController(title: "settings_alert_delete_title".localized(), message: "settings_alert_delete_subtitle".localized(), preferredStyle: .actionSheet)
        let deleteAction = UIAlertAction(title: "settings_alert_delete_agreement".localized(), style: .destructive) { (alert) in
            self.deleteUser()
        }
        let cancelAction = UIAlertAction(title: "settings_alert_delete_cancel".localized(), style: .cancel) { (alert) in
            
        }
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(cancelAction)
        
        if ( UI_USER_INTERFACE_IDIOM() == .pad ){
            if let currentPopoverpresentioncontroller = optionMenu.popoverPresentationController{
                currentPopoverpresentioncontroller.sourceView = btnDelete
                currentPopoverpresentioncontroller.sourceRect = btnDelete.bounds
                currentPopoverpresentioncontroller.permittedArrowDirections = .up
                self.present(optionMenu, animated: true, completion: nil)
            }
        }else{
            self.present(optionMenu, animated: true, completion: nil)
        }
    }
    
    func makeTransitionToLogin() {
        if let controller = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as? LoginVC{
            presentDetail(controller)
        }
    }
    
    func setupUI() {
        let currentUser = User.currentUser()!
        lblName.text = currentUser.fullname
        if currentUser.avatar != "" {
            imageFromData(pictureData: currentUser.avatar) { (avatarImage) in
                if avatarImage != nil {
                    self.ivAvatar.image = avatarImage!.circleMasked
                }
            }
        }
//        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
//        }
    }
    
    func deleteUser() {
        userDefaults.removeObject(forKey: PUSHID)
        userDefaults.removeObject(forKey: CURRENTUSER)
        userDefaults.synchronize()
        reference(.User).document(User.currentId()).delete()
        User.deleteUser { (error) in
            if error != nil {
                DispatchQueue.main.async {
                    ProgressHUD.showError(error?.localizedDescription)
                }
                return
            }
            self.makeTransitionToLogin()
        }
    }
    
    func saveUserDefaults() {
        userDefaults.set(avatarSwitchStatus, forKey: SHOWAVATAR)
        userDefaults.synchronize()
    }
    
    func loadUserDefaults() {
        firstLoad = userDefaults.bool(forKey: FIRSTRUN)
        if !firstLoad! {
            userDefaults.set(true, forKey: FIRSTRUN)
            userDefaults.set(avatarSwitchStatus, forKey: SHOWAVATAR)
            userDefaults.synchronize()
        }
        avatarSwitchStatus = userDefaults.bool(forKey: SHOWAVATAR)
        switchAvatars.isOn = avatarSwitchStatus
    }
}
