//
//  RegisterVC.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/6/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit
import ProgressHUD
import ImagePicker

class RegisterVC: UIViewController, ImagePickerDelegate {

    @IBOutlet weak var tfPhone: AppTextField!
    @IBOutlet weak var tfCity: AppTextField!
    @IBOutlet weak var tfCountry: AppTextField!
    @IBOutlet weak var tfSurname: AppTextField!
    @IBOutlet weak var tfName: AppTextField!
    @IBOutlet weak var tfEmail: AppTextField!
    @IBOutlet weak var tfPassword: AppTextField!
    @IBOutlet weak var tfRePassword: AppTextField!
    @IBOutlet weak var ivAvatar: CircleImageView!

    var imageAvatar: UIImage?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(choosePhoto))
        tap.cancelsTouchesInView = false
        ivAvatar.isUserInteractionEnabled = true
        ivAvatar.addGestureRecognizer(tap)
    }

    @objc func choosePhoto() {
        let imagePickerController = ImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 1
        present(imagePickerController, animated: true, completion: nil)
        dismissKeyboard()
    }

    @IBAction func onDoneClicked(_ sender: Any) {
        if(tfPassword.text! != tfRePassword.text!){
            ProgressHUD.showError("register_field_passwords_are_diff".localized())
        }else if tfName.text == ""{
            ProgressHUD.showError("register_field_name_empty".localized())
        }else if tfSurname.text == ""{
            ProgressHUD.showError("register_field_surname_empty".localized())
        }else if tfEmail.text == ""{
            ProgressHUD.showError("register_field_email_empty".localized())
        }else if tfPassword.text == ""{
            ProgressHUD.showError("register_field_pass_empty".localized())
        }
//        else if tfCountry.text == ""{
//            ProgressHUD.showError("register_field_country_empty".localized())
//        }else if tfCity.text == ""{
//            ProgressHUD.showError("register_field_city_empty".localized())
//        }
//        else if tfPhone.text == ""{
//            ProgressHUD.showError("register_field_phone_empty".localized())
//        }
        else{
            ProgressHUD.show("register_loading_message".localized())
            User.registerUserWith(email: tfEmail.text!, password: tfPassword.text!, firstName: tfName.text!, lastName: tfSurname.text!) { (error) in
                print("onDoneClicked \(error?.localizedDescription ?? "none")")
                if error != nil {
                    ProgressHUD.showError(error?.localizedDescription)
                    return
                }
                self.registerUser()
            }
        }
    }

    @IBAction func onCancelClicked(_ sender: Any) {
        dismissDetail()
    }

    func registerUser() {
        let fullName = tfName.text! + " " + tfSurname.text!
        var tempDict: Dictionary = [FIRSTNAME: tfName.text!, LASTNAME: tfSurname.text!,
                                    FULLNAME: fullName, COUNTRY: tfCountry.text!,
                                    CITY: tfCity.text!, PHONE: tfPhone.text!] as [String: Any]

        if imageAvatar == nil {
            imageFromInitials(firstName: tfName.text!, lastName: tfSurname.text!) { (image) in
                tempDict[AVATAR] = getBase64FromImage(image: image)
            }
        } else {
            tempDict[AVATAR] = getBase64FromImage(image: imageAvatar!)
        }
        finishRegistration(values: tempDict)
    }

    func finishRegistration(values: [String: Any]) {
        updateCurrentUserInFirestore(withValues: values) { (error) in
            if error != nil {
                DispatchQueue.main.async(execute: { () -> Swift.Void in
                    print("finishRegistration \(error?.localizedDescription ?? "none")")
                    ProgressHUD.showError(error?.localizedDescription)
                })
                return
            }
            self.makeTransitionToMain()
        }
    }

    func makeTransitionToMain() {
        ProgressHUD.dismiss()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: USER_DID_LOGIN_NOTIFICATION), object: nil, userInfo: [USERID: User.currentId()])
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "MainVC") {
            self.presentDetail(controller)
        }
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        if images.count > 0 {
            self.imageAvatar = images.first!
            self.ivAvatar.image = self.imageAvatar?.circleMasked
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}
