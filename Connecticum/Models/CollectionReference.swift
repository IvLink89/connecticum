//
//  CollectionReference.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/5/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import Foundation
import FirebaseFirestore

enum CollectionReferenceType: String {
    case User
    case Typing
    case Chat
    case Message
    case Group
    case Call
}

func reference(_ collectionReference: CollectionReferenceType) -> CollectionReference {
    return Firestore.firestore().collection(collectionReference.rawValue)
}
