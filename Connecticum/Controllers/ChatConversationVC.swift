//
//  ChatConversationVC.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/15/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import IQAudioRecorderController
import ProgressHUD
import IDMPhotoBrowser
import AVFoundation
import AVKit
import FirebaseFirestore

class ChatConversationVC: JSQMessagesViewController,
IQAudioRecorderViewControllerDelegate,
UIImagePickerControllerDelegate,
UINavigationControllerDelegate{

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var outgoingBubble = JSQMessagesBubbleImageFactory()?.outgoingMessagesBubbleImage(with: #colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1))
    var incomingBubble = JSQMessagesBubbleImageFactory()?.incomingMessagesBubbleImage(with: #colorLiteral(red: 0, green: 0.5988745093, blue: 0.9821230769, alpha: 1))
    
    var typingCounter = 0
    
    var chatRoomId:String!
    var membersIds: [String]!
    var membersToPush: [String]!
    var titleName: String! = ""
    
    let messagesTypes = [AUDIO, VIDEO, TEXT, LOCATION, PICTURE]
    
    var messages: [JSQMessage] = []
    var objectMessages: [NSDictionary] = []
    var loadedMessages: [NSDictionary] = []
    var allPictureMessages: [String] = []
    var initialLoadComplete = false
    
    var maxMessageNumber = 0
    var minMessageNumber = 0
    var loadedMessagesCount = 0
    var loadOld = false
    
    var typingListener: ListenerRegistration?
    var updatedChatListener: ListenerRegistration?
    var messageListener: ListenerRegistration?
    
    var jsqAvatarDict: NSMutableDictionary?
    var avatarImageDict: NSMutableDictionary?
    var showAvatars = true
    var firstLoad: Bool?
    var isGroup:Bool?
    var group:NSDictionary?
    var withUsers: [User] = []
    
    let leftBarButtonView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 240, height: 44))
        return view
    }()
    let avatarButton: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 11, width: 25, height: 25))
        return button
    }()
    let titleLabel: UILabel = {
        let title = UILabel(frame: CGRect(x: 33, y: 11, width: 196, height: 15))
        title.textAlignment = .left
        title.font = UIFont(name: "Avenir-Medium", size: 14)
        return title
    }()
    let subTitleLabel: UILabel = {
        let subTitle = UILabel(frame: CGRect(x: 33, y: 26, width: 140, height: 15))
        subTitle.textAlignment = .left
        subTitle.font = UIFont(name: "Avenir-Light", size: 11)
        return subTitle
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        clearChatCounter(chatRoomId: chatRoomId)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        clearChatCounter(chatRoomId: chatRoomId)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        senderId = User.currentId()
        senderDisplayName = User.currentUser()?.fullname
        
        JSQMessagesCollectionViewCell.registerMenuAction(#selector(delete))
        
        collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        
        jsqAvatarDict = [:]
        collectionView.loadEarlierMessagesHeaderTextColor = #colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1)
        
        if isGroup! {
            getCurrentGroup(withId: chatRoomId)
        }
        
        navigationItem.largeTitleDisplayMode = .never
        navigationItem.leftBarButtonItems = [UIBarButtonItem(image: UIImage(named: "Back"), style: .plain, target: self, action: #selector(backClicked))]
        
        inputToolbar.contentView.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.937254902, blue: 0.9450980392, alpha: 1)
        inputToolbar.contentView.rightBarButtonItem.setTitle("", for: .normal)
        inputToolbar.contentView.rightBarButtonItem.setImage(UIImage(named: "mic"), for: .normal)
        
        setToolbar()
        createTypingObserver()
        loadUserDefaults()
        
        loadMessages()
    }
    
    @objc func backClicked() {
        clearChatCounter(chatRoomId: chatRoomId)
        removeListeners()
        navigationController?.popViewController(animated: true)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        let data = messages[indexPath.row]
        
        if cell.textView != nil{
            cell.textView.font = UIFont(name: "Avenir-Medium", size: 18)
            if data.senderId == User.currentId() {
                cell.textView?.textColor = .white
            } else {
                cell.textView?.textColor = .black
            }
        }

        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.row]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let data = messages[indexPath.row]
        if data.senderId == User.currentId() {
            return outgoingBubble
        } else {
            return incomingBubble
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        if indexPath.item % 3 == 0 {
            let message = messages[indexPath.row]
            return JSQMessagesTimestampFormatter.shared()?.attributedTimestamp(for: message.date)
        }
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAt indexPath: IndexPath!) -> CGFloat {
        if indexPath.item % 3 == 0 {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        }
        return 0.0
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellBottomLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let message = objectMessages[indexPath.row]
        let status: NSAttributedString!
        let attributedStringColor = [NSAttributedString.Key.foregroundColor : UIColor.black]

        switch message[STATUS] as! String {
        case DELIVERED:
            status = NSAttributedString(string: DELIVERED)
        case READ:
            let statusText = "Read" + " " + readTimeFrom(dateString: message[READDATE] as! String)
            status = NSAttributedString(string: statusText, attributes: attributedStringColor)
        default:
            status = NSAttributedString(string: "✔︎")
        }

        if indexPath.row == (messages.count - 1) {
            return status
        } else {
            return NSAttributedString(string: "")
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
        let data = messages[indexPath.row]
        if data.senderId == User.currentId() {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        } else {
            return 0.0
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        let message = messages[indexPath.row]
        var avatar: JSQMessageAvatarImageDataSource
        
        if let testAvatar = jsqAvatarDict!.object(forKey: message.senderId) {
            avatar = testAvatar as! JSQMessageAvatarImageDataSource
        } else {
            avatar = JSQMessagesAvatarImageFactory.avatarImage(with: UIImage(named: "avatarPlaceholder"), diameter: 70)
        }
        return avatar
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, header headerView: JSQMessagesLoadEarlierHeaderView!, didTapLoadEarlierMessagesButton sender: UIButton!) {
        loadMoreMessages(maxNumber: maxMessageNumber, minNumber: minMessageNumber)
        self.collectionView.reloadData()
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        let messageDictionary = objectMessages[indexPath.row]
        let messageType = messageDictionary[TYPE] as! String
        
        switch messageType {
        case PICTURE:
            let message = messages[indexPath.row]
            let mediaItem = message.media as! JSQPhotoMediaItem
            let photos = IDMPhoto.photos(withImages: [mediaItem.image])
            let browser = IDMPhotoBrowser(photos: photos)
            self.present(browser!, animated: true, completion: nil)
        case VIDEO:
            let message = messages[indexPath.row]
            let mediaItem = message.media as! ChatVideoItem
            let player = AVPlayer(url: mediaItem.fileURL! as URL)
            let moviewPlayer = AVPlayerViewController()
            let session = AVAudioSession.sharedInstance()
            try! session.setCategory(.playAndRecord, mode: .default, options: .defaultToSpeaker)
            moviewPlayer.player = player
            self.present(moviewPlayer, animated: true) {
                moviewPlayer.player!.play()
            }
        case LOCATION:
            print("LOCATION")
            let message = messages[indexPath.row]
            let mediaItem = message.media as! JSQLocationMediaItem
            if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MapVC") as? MapVC {
                print("controller \(controller)")
                controller.location = mediaItem.location
                self.navigationController?.pushViewController(controller, animated: true)
            }
        default:
            print("Unknown tapped")
        }
    }
    
    override func didPressAccessoryButton(_ sender: UIButton!) {
        let alertSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cameraHelper = CameraHelper(delegate: self)
        let actionCamera = UIAlertAction(title: "chat_alert_camera".localized(), style: .default) { (action) in
            cameraHelper.presentMultyCamera(target: self, canEdit: false)
        }
        let actionPhoto = UIAlertAction(title: "chat_alert_photo_lib".localized(), style: .default) { (action) in
            cameraHelper.presentPhotoLibrary(target: self, canEdit: false)
        }
        let actionVideo = UIAlertAction(title: "chat_alert_video_lib".localized(), style: .default) { (action) in
            cameraHelper.presentVideoLibrary(target: self, canEdit: false)
        }
        let actionLocation = UIAlertAction(title: "chat_alert_share_loc".localized(), style: .default) { (action) in
            if self.haveAccessToUserLocation() {
                self.sendMessage(text: nil, date: Date(), picture: nil, location: LOCATION, video: nil, audio: nil)
            }
        }
        let actionCancel = UIAlertAction(title: "chat_alert_cancel".localized(), style: .cancel) { (action) in
            
        }
        actionCamera.setValue(#colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1), forKey: "titleTextColor")
        actionPhoto.setValue(#colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1), forKey: "titleTextColor")
        actionVideo.setValue(#colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1), forKey: "titleTextColor")
        actionLocation.setValue(#colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1), forKey: "titleTextColor")
        actionCancel.setValue(#colorLiteral(red: 0.9568627451, green: 0.262745098, blue: 0.2117647059, alpha: 1), forKey: "titleTextColor")
        
        actionCamera.setValue(UIImage(named: "camera"), forKey: "image")
        actionPhoto.setValue(UIImage(named: "picture"), forKey: "image")
        actionVideo.setValue(UIImage(named: "video"), forKey: "image")
        actionLocation.setValue(UIImage(named: "location"), forKey: "image")
        
        alertSheet.addAction(actionCamera)
        alertSheet.addAction(actionPhoto)
        alertSheet.addAction(actionVideo)
        alertSheet.addAction(actionLocation)
        alertSheet.addAction(actionCancel)
        
        if ( UI_USER_INTERFACE_IDIOM() == .pad ){
            if let currentPopoverpresentioncontroller = alertSheet.popoverPresentationController{
                currentPopoverpresentioncontroller.sourceView = self.inputToolbar.contentView.leftBarButtonItem
                currentPopoverpresentioncontroller.sourceRect = self.inputToolbar.contentView.leftBarButtonItem.bounds
                currentPopoverpresentioncontroller.permittedArrowDirections = .up
                self.present(alertSheet, animated: true, completion: nil)
            }
        }else{
            self.present(alertSheet, animated: true, completion: nil)
        }
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        if text != "" {
            self.sendMessage(text: text, date: date, picture: nil, location: nil, video: nil, audio: nil)
            updateSendButton(isSend: false)
        } else {
            let audioHelper = AudioHelper(delegate_: self)
            audioHelper.presentAudioRecorder(target: self)
        }
    }
    
    override func textViewDidChange(_ textView: UITextView) {
        if textView.text != "" {
            updateSendButton(isSend: true)
        } else {
            updateSendButton(isSend: false)
        }
    }
    
    override func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        typingCounterStart()
        return true
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapAvatarImageView avatarImageView: UIImageView!, at indexPath: IndexPath!) {
        let senderId = messages[indexPath.row].senderId
        var currUser: User?
        if senderId == User.currentId() {
            currUser = User.currentUser()
        } else {
            for user in withUsers {
                if user.objectId == senderId {
                    currUser = user
                }
            }
        }
        presentUserProfile(forUser: currUser!)
    }
    
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        super.collectionView(collectionView, shouldShowMenuForItemAt: indexPath)
        return true
    }
    
    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        if messages[indexPath.row].isMediaMessage {
            if action.description == "delete:" {
                return true
            } else {
                return false
            }
        } else {
            if action.description == "delete:" || action.description == "copy:" {
                return true
            } else {
                return false
            }
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didDeleteMessageAt indexPath: IndexPath!) {
        let messageId = objectMessages[indexPath.row][MESSAGEID] as! String
        objectMessages.remove(at: indexPath.row)
        messages.remove(at: indexPath.row)
        OutgoingMessage.deleteMessage(withId: messageId, chatRoomId: chatRoomId)
    }
    
    func updateSendButton(isSend: Bool) {
        if isSend {
            self.inputToolbar.contentView.rightBarButtonItem.setImage(UIImage(named: "send"), for: .normal)
        } else {
            self.inputToolbar.contentView.rightBarButtonItem.setImage(UIImage(named: "mic"), for: .normal)
        }
    }
    
    func sendMessage(text: String?, date: Date, picture: UIImage?, location: String?, video: NSURL?, audio: String?) {
        var outgoingMessage: OutgoingMessage?
        let currUser = User.currentUser()!
        
        if let text = text {
            let ecryptedText = CryptoTextHelper.encryptText(chatRoomId: chatRoomId, message: text)
            outgoingMessage = OutgoingMessage(message: ecryptedText, senderId: currUser.objectId, senderName: currUser.fullname, date: date, status: DELIVERED, type: TEXT)
        }
        
        if let pic = picture {
            uploadImage(image: pic, chatRoomId: chatRoomId, view: self.navigationController!.view) { (imageLink) in
                if imageLink != nil {
                    let ecryptedText = CryptoTextHelper.encryptText(chatRoomId: self.chatRoomId, message: "[\(PICTURE)]")
                    outgoingMessage = OutgoingMessage(message: ecryptedText, pictureLink: imageLink!, senderId: currUser.objectId, senderName: currUser.firstname, date: date, status: DELIVERED, type: PICTURE)
                    
                    JSQSystemSoundPlayer.jsq_playMessageSentSound()
                    self.finishSendingMessage()
                    outgoingMessage?.sendMessage(chatRoomID: self.chatRoomId, messageDict: outgoingMessage!.messageDict, memberIds: self.membersIds, membersToPush: self.membersToPush)
                }
            }
            return
        }
        
        if let video = video {
            let videoData = NSData(contentsOfFile: video.path!)
            let dataThumbnail = videoThumbnail(video: video).jpegData(compressionQuality: 0.5)
            uploadVideo(video: videoData!, chatRoomId: chatRoomId, view: self.navigationController!.view) { (videoLink) in
                if videoLink != nil {
                    let ecryptedText = CryptoTextHelper.encryptText(chatRoomId: self.chatRoomId, message: "[\(VIDEO)]")
                    outgoingMessage = OutgoingMessage(message: ecryptedText, video: videoLink!, thumbNail: dataThumbnail! as NSData, senderId: currUser.objectId, senderName: currUser.firstname, date: date, status: DELIVERED, type: VIDEO)
                    
                    JSQSystemSoundPlayer.jsq_playMessageSentSound()
                    self.finishSendingMessage()
                    outgoingMessage?.sendMessage(chatRoomID: self.chatRoomId, messageDict: outgoingMessage!.messageDict, memberIds: self.membersIds, membersToPush: self.membersToPush)
                    
                }
            }
            return
        }
        
        if let audioPath = audio {
            uploadAudio(autioPath: audioPath, chatRoomId: chatRoomId, view: (self.navigationController?.view)!) { (audioLink) in
                if audioLink != nil {
                    let encryptedText = CryptoTextHelper.encryptText(chatRoomId: self.chatRoomId, message: "[\(AUDIO)]")
                    outgoingMessage = OutgoingMessage(message: encryptedText, audio: audioLink!, senderId: currUser.objectId, senderName: currUser.firstname, date: date, status: DELIVERED, type: AUDIO)
                    
                    JSQSystemSoundPlayer.jsq_playMessageSentSound()
                    self.finishSendingMessage()
                    outgoingMessage!.sendMessage(chatRoomID: self.chatRoomId, messageDict: outgoingMessage!.messageDict, memberIds: self.membersIds, membersToPush: self.membersToPush)
                }
            }
            return
        }
        
        if location != nil {
            let lat: NSNumber = NSNumber(value: appDelegate.coordinates!.latitude)
            let long: NSNumber = NSNumber(value: appDelegate.coordinates!.longitude)
            let ecryptedText = CryptoTextHelper.encryptText(chatRoomId: chatRoomId, message: "[\(LOCATION)]")
            outgoingMessage = OutgoingMessage(message: ecryptedText, latitude: lat, longitude: long, senderId: currUser.objectId, senderName: currUser.firstname, date: date, status: DELIVERED, type: LOCATION)
        }
        
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        finishSendingMessage(animated: true)
        
        print("chatRoomID \(chatRoomId) - \(outgoingMessage?.messageDict)")
        outgoingMessage?.sendMessage(chatRoomID: chatRoomId, messageDict: outgoingMessage?.messageDict ?? NSMutableDictionary(), memberIds: membersIds, membersToPush: membersToPush)
    }
    
    func loadMessages() {
        updatedChatListener = reference(.Message).document(User.currentId()).collection(chatRoomId).addSnapshotListener({ (snapshot, error) in
            guard let snapshot = snapshot else { return }
            if !snapshot.isEmpty {
                snapshot.documentChanges.forEach({ (diff) in
                    if diff.type == .modified {
                        self.updateMessage(messageDictionary: diff.document.data() as NSDictionary)
                    }
                })
            }
        })

        reference(.Message).document(User.currentId()).collection(chatRoomId).order(by: DATE, descending: true).limit(to: NUMBEROFMESSAGES+1).getDocuments { (snapshot, error) in
            
            guard let snapshot = snapshot else {
                self.initialLoadComplete = true
                self.observeNewMessages()
                return
            }
            
            let sorted = ((dictionaryFromSnapshots(snapshots: snapshot.documents)) as NSArray).sortedArray(using: [NSSortDescriptor(key: DATE, ascending: true)]) as! [NSDictionary]
            
            self.loadedMessages = self.removeBadMessages(allMessages: sorted)
            print("COUNT \(self.loadedMessages.count)")
            self.insertMessages()
            self.finishReceivingMessage(animated: true)
            self.initialLoadComplete = true
            self.getPictureMessages()
            self.getOldMessagesInBackground()
            self.observeNewMessages()
        }
    }
    
    func loadMoreMessages(maxNumber: Int, minNumber: Int) {
        if loadOld {
            maxMessageNumber = minNumber - 1
            minMessageNumber = maxMessageNumber - NUMBEROFMESSAGES
        }
        if minMessageNumber < 0 {
            minMessageNumber = 0
        }
        for i in (minMessageNumber ... maxMessageNumber).reversed() {
            let messageDictionary = loadedMessages[i]
            insertNewMessage(messageDictionary: messageDictionary)
            loadedMessagesCount += 1
        }
        loadOld = true
        self.showLoadEarlierMessagesHeader = (loadedMessagesCount != loadedMessages.count)
    }
    
    func insertNewMessage(messageDictionary: NSDictionary) {
        let incomingMessage = IncomingMessage(collectionView: self.collectionView!)
        let message = incomingMessage.createMessage(messageDictionary: messageDictionary, chatRoomId: chatRoomId)
        objectMessages.insert(messageDictionary, at: 0)
        messages.insert(message!, at: 0)
    }
    
    func removeBadMessages(allMessages: [NSDictionary]) -> [NSDictionary] {
        var tempMessages = allMessages
        print("removeBadMessages \(tempMessages.count)")
        for message in tempMessages {
            if message[TYPE] != nil {
                if !self.messagesTypes.contains(message[TYPE] as! String) {
                    tempMessages.remove(at: tempMessages.firstIndex(of: message)!)
                }
            } else {
                tempMessages.remove(at: tempMessages.firstIndex(of: message)!)
            }
        }
        return tempMessages
    }
    
    func insertMessages() {
        maxMessageNumber = loadedMessages.count - loadedMessagesCount
        minMessageNumber = maxMessageNumber - NUMBEROFMESSAGES
        if minMessageNumber < 0 {
            minMessageNumber = 0
        }
        
        for i in minMessageNumber ..< maxMessageNumber {
            let messageDictionary = loadedMessages[i]
            insertInitialLoadMessages(messageDictionary: messageDictionary)
            loadedMessagesCount += 1
        }
        
        self.showLoadEarlierMessagesHeader = (loadedMessagesCount != loadedMessages.count)
    }
    
    func insertInitialLoadMessages(messageDictionary: NSDictionary) -> Bool {
        let incomingMessage = IncomingMessage(collectionView: self.collectionView!)

        if (messageDictionary[SENDERID] as! String) != User.currentId() {
            OutgoingMessage.updateMessage(withId: messageDictionary[MESSAGEID] as! String, chatRoomId: chatRoomId, memberIds: membersIds)
        }
        let message = incomingMessage.createMessage(messageDictionary: messageDictionary, chatRoomId: chatRoomId)
        if message != nil {
            objectMessages.append(messageDictionary)
            messages.append(message!)
        }

        return isIncoming(messageDictionary: messageDictionary)
    }
    
    func isIncoming(messageDictionary: NSDictionary) -> Bool {
        if User.currentId() == messageDictionary[SENDERID] as! String {
            return false
        } else {
            return true
        }
    }
    
    func observeNewMessages() {
        var lastMessageDate = "0"
        if loadedMessages.count > 0 {
            lastMessageDate = loadedMessages.last![DATE] as! String
        }
        
        messageListener = reference(.Message).document(User.currentId()).collection(chatRoomId).whereField(DATE, isGreaterThan: lastMessageDate).addSnapshotListener({ (snapshot, error) in
            guard let snapshot = snapshot else { return }
            
            if !snapshot.isEmpty {
                for diff in snapshot.documentChanges {
                    if (diff.type == .added) {
                        
                        let item = diff.document.data() as NSDictionary
                        
                        if let type = item[TYPE] {
                            if self.messagesTypes.contains(type as! String) {
                                if type as! String == PICTURE {
                                    self.addNewPictureMessageLink(link: item[PICTURE] as! String)
                                }
                                if self.insertInitialLoadMessages(messageDictionary: item) {
                                    JSQSystemSoundPlayer.jsq_playMessageReceivedSound()
                                }
                                self.finishReceivingMessage()
                            }
                        }
                    }
                }
            }
        })
    }
    
    func getOldMessagesInBackground() {
        print("COUNT \(loadedMessages.count)")
        if loadedMessages.count > NUMBEROFMESSAGES {
            let firstMessageDate = loadedMessages.first![DATE] as! String
            
            reference(.Message).document(User.currentId()).collection(chatRoomId).whereField(DATE, isLessThan: firstMessageDate).getDocuments { (snapshot, error) in
                guard let snapshot = snapshot else { return }
                
                let sorted = ((dictionaryFromSnapshots(snapshots: snapshot.documents)) as NSArray).sortedArray(using: [NSSortDescriptor(key: DATE, ascending: true)]) as! [NSDictionary]
                self.loadedMessages = self.removeBadMessages(allMessages: sorted) + self.loadedMessages
                self.getPictureMessages()
                self.maxMessageNumber = self.loadedMessages.count - self.loadedMessagesCount - 1
                self.minMessageNumber = self.maxMessageNumber - NUMBEROFMESSAGES
                print("maxMessageNumber \(self.maxMessageNumber)")
                print("minMessageNumber \(self.minMessageNumber)")
            }
        }
    }
    
    func updateMessage(messageDictionary: NSDictionary) {
        for index in 0 ..< objectMessages.count {
            let temp = objectMessages[index]

            if messageDictionary[MESSAGEID] as! String == temp[MESSAGEID] as! String {
                objectMessages[index] = messageDictionary
                self.collectionView!.reloadData()
            }
        }
    }
    
    func setToolbar() {
        leftBarButtonView.addSubview(avatarButton)
        leftBarButtonView.addSubview(titleLabel)
        leftBarButtonView.addSubview(subTitleLabel)
        
//        let infoButton = UIBarButtonItem(image: UIImage(named: "info"), style: .plain, target: self, action: #selector(self.infoButtonPressed))
//        self.navigationItem.rightBarButtonItem = infoButton
        
        let leftBarButtonItem = UIBarButtonItem(customView: leftBarButtonView)
        self.navigationItem.leftBarButtonItems?.append(leftBarButtonItem)
        
        if isGroup ?? false {
            avatarButton.addTarget(self, action: #selector(self.showGroup), for: .touchUpInside)
        } else {
            avatarButton.addTarget(self, action: #selector(self.showUserProfile), for: .touchUpInside)
        }
        
        getUsersFromFirestore(withIds: membersIds) { (withUsers) in
            self.withUsers = withUsers
            self.getAvatarImages()
            if !(self.isGroup ?? false) {
                self.setUIForSingleChat()
            }
        }
    }
    
    @objc func infoButtonPressed() {
        let picturesVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PicturesVC") as! PicturesVC
        print("infoButtonPressed \(allPictureMessages)")
        picturesVC.allImageLinks = allPictureMessages
        self.navigationController?.pushViewController(picturesVC, animated: true)
    }
    
    @objc func showGroup() {
        let groupVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GroupVC") as! GroupVC
        groupVC.group = group!
        self.navigationController?.pushViewController(groupVC, animated: true)
    }
    
    @objc func showUserProfile() {
        if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileVC") as? ProfileVC {
            controller.user = withUsers.first
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func presentUserProfile(forUser: User) {
        let profileVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        profileVC.user = forUser
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    func getAvatarImages() {
        if showAvatars {
            collectionView?.collectionViewLayout.incomingAvatarViewSize = CGSize(width: 30, height: 30)
            collectionView?.collectionViewLayout.outgoingAvatarViewSize = CGSize(width: 30, height: 30)
            
            getUserAvatar(user: User.currentUser()!)
            for user in withUsers {
                getUserAvatar(user: user)
            }
        }
    }
    
    func getUserAvatar(user: User) {
        if user.avatar != "" {
            dataImageFromString(pictureString: user.avatar) { (imageData) in
                if imageData == nil {
                    return
                }
                if self.avatarImageDict != nil {
                    self.avatarImageDict!.removeObject(forKey: user.objectId)
                    self.avatarImageDict!.setObject(imageData!, forKey: user.objectId as NSCopying)
                } else {
                    self.avatarImageDict = [user.objectId : imageData!]
                }
                self.createJSQAvatars(avatarDict: self.avatarImageDict)
            }
        }
    }
    
    func createJSQAvatars(avatarDict: NSMutableDictionary?) {
        let defaultAvatar = JSQMessagesAvatarImageFactory.avatarImage(with: UIImage(named: "avatarPlaceholder"), diameter: 70)
        if avatarDict != nil {
            for userId in membersIds {
                if let avatarImageData = avatarDict![userId] {
                    let jsqAvatar = JSQMessagesAvatarImageFactory.avatarImage(with: UIImage(data: avatarImageData as! Data), diameter: 70)
                    self.jsqAvatarDict!.setValue(jsqAvatar, forKey: userId)
                } else {
                    self.jsqAvatarDict!.setValue(defaultAvatar, forKey: userId)
                }
            }
            self.collectionView.reloadData()
        }
    }
    
    func setUIForSingleChat() {
        let withUser = withUsers.first!
        imageFromData(pictureData: withUser.avatar) { (image) in
            if image != nil {
                avatarButton.setImage(image!.circleMasked, for: .normal)
            }
        }
        titleLabel.text = withUser.fullname
        if withUser.isOnline {
            subTitleLabel.text = "chat_online".localized()
        } else {
            subTitleLabel.text = "chat_offline".localized()
        }
        avatarButton.addTarget(self, action: #selector(self.showUserProfile), for: .touchUpInside)
    }
    
    func setUIForGroupChat() {
        imageFromData(pictureData: (group![AVATAR] as! String)) { (image) in
            if image != nil {
                avatarButton.setImage(image!.circleMasked, for: .normal)
            }
        }
        titleLabel.text = titleName
        subTitleLabel.text = ""
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let video = info[UIImagePickerController.InfoKey.mediaURL] as? NSURL
        let picture = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        
        print("imagePickerController \(picture)")
        sendMessage(text: nil, date: Date(), picture: picture, location: nil, video: video, audio: nil)
        picker.dismiss(animated: true, completion: nil)
    }
    
    func audioRecorderController(_ controller: IQAudioRecorderViewController, didFinishWithAudioAtPath filePath: String) {
        controller.dismiss(animated: true, completion: nil)
        self.sendMessage(text: nil, date: Date(), picture: nil, location: nil, video: nil, audio: filePath)
    }
    
    func audioRecorderControllerDidCancel(_ controller: IQAudioRecorderViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func haveAccessToUserLocation() -> Bool {
        if appDelegate.locationManager != nil {
            return true
        } else {
            ProgressHUD.showError("chat_location_alert".localized())
            return false
        }
    }
    
    func createTypingObserver() {
        typingListener = reference(.Typing).document(chatRoomId).addSnapshotListener({ (snapshot, error) in
            guard let snapshot = snapshot else { return }
            if snapshot.exists {
                for data in snapshot.data()! {
                    if data.key != User.currentId() {
                        let typing = data.value as! Bool
                        self.showTypingIndicator = typing
                        if typing {
                            self.scrollToBottom(animated: true)
                        }
                    }
                }
            } else {
                reference(.Typing).document(self.chatRoomId).setData([User.currentId() : false])
            }
        })
    }
    
    func typingCounterStart() {
        typingCounter += 1
        typingCounterSave(typing: true)
        self.perform(#selector(self.typingCounterStop), with: nil, afterDelay: 2.0)
    }
    
    @objc func typingCounterStop() {
        typingCounter -= 1
        if typingCounter == 0 {
            typingCounterSave(typing: false)
        }
    }
    
    func typingCounterSave(typing: Bool) {
        reference(.Typing).document(chatRoomId).updateData([User.currentId() : typing])
    }
    
    func removeListeners() {
        if typingListener != nil {
            typingListener!.remove()
        }
        if messageListener != nil {
            messageListener!.remove()
        }
        if updatedChatListener != nil {
            updatedChatListener!.remove()
        }
    }
    
    func getCurrentGroup(withId: String) {
        reference(.Group).document(withId).getDocument { (snapshot, error) in
            guard let snapshot = snapshot else { return }
            if snapshot.exists {
                self.group = snapshot.data() as? NSDictionary
                self.setUIForGroupChat()
            }
        }
    }
    
    func loadUserDefaults() {
        firstLoad = userDefaults.bool(forKey: FIRSTRUN)
        if !firstLoad! {
            userDefaults.set(true, forKey: FIRSTRUN)
            userDefaults.set(showAvatars, forKey: SHOWAVATAR)
            userDefaults.synchronize()
        }
        showAvatars = userDefaults.bool(forKey: SHOWAVATAR)
        checkForBackgroundImage()
    }
    
    func addNewPictureMessageLink(link: String) {
        allPictureMessages.append(link)
    }
    
    func getPictureMessages() {
        allPictureMessages = []
        for message in loadedMessages {
            if message[TYPE] as! String == PICTURE {
                allPictureMessages.append(message[PICTURE] as! String)
            }
        }
    }
    
    func checkForBackgroundImage() {
        if userDefaults.object(forKey: BACKGROUBNDIMAGE) != nil {
            self.collectionView.backgroundColor = .clear
            print("WIDTH \(self.view.bounds.width)")
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width*2, height: self.view.bounds.height*2))
            imageView.image = UIImage(named: userDefaults.object(forKey: BACKGROUBNDIMAGE) as! String)!
            imageView.contentMode = .scaleAspectFit
            self.view.insertSubview(imageView, at: 0)
        }
    }
}
