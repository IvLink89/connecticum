//
//  ControllerExtensions.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/2/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import Foundation
import UIKit
import JSQMessagesViewController

extension UIViewController{
    
    func presentDetail(_ viewController: UIViewController){
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromRight
        self.view.window?.layer.add(transition, forKey: kCATransition)
        
        present(viewController, animated: false, completion: nil)
    }
    
    func dismissDetail(){
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromLeft
        self.view.window?.layer.add(transition, forKey: kCATransition)
        
        dismiss(animated: false, completion: nil)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension JSQMessagesInputToolbar {
    override open func didMoveToWindow() {
        super.didMoveToWindow()
        if #available(iOS 11.0, *), let window = self.window {
            let anchor = window.safeAreaLayoutGuide.bottomAnchor
            bottomAnchor.constraint(lessThanOrEqualToSystemSpacingBelow: anchor, multiplier: 1.0).isActive = true
        }
    }
    
}
//extension UINavigationController{
//    
//    func presentNavDetail(_ viewController: UIViewController){
//        let transition = CATransition()
//        transition.duration = 0.6
//        transition.type = CATransitionType.reveal
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        transition.subtype = CATransitionSubtype.fromRight
//
//        self.view.layer.add(transition, forKey: kCATransition)
//        self.pushViewController(viewController, animated: false)
//    }
//    
//    func dismissNavDetail(){
//        let transition = CATransition()
//        transition.duration = 0.6
//        transition.type = CATransitionType.reveal
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        transition.subtype = CATransitionSubtype.fromLeft
//        
//        self.view.layer.add(transition, forKey: kCATransition)
//        self.popViewController(animated: false)
//    
//    }
//}
