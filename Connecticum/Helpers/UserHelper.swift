//
//  UserHelper.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/5/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import Foundation

func saveUserToFirestore(user: User) {
    reference(.User).document(user.objectId).setData(userDictionaryFrom(user: user) as! [String : Any]) { (error) in
        print("error is \(error?.localizedDescription ?? "")")
    }
}

func saveUserLocally(user: User) {
    UserDefaults.standard.set(userDictionaryFrom(user: user), forKey: CURRENTUSER)
    UserDefaults.standard.synchronize()
}

func fetchCurrentUserFromFirestore(userId: String) {
    reference(.User).document(userId).getDocument { (snapshot, error) in
        guard let snapshot = snapshot else {  return }
        if snapshot.exists {
            print("updated current users params \(snapshot.metadata) \n ---------------- \(snapshot.data()!)")
            UserDefaults.standard.setValue(snapshot.data()! as NSDictionary, forKeyPath: CURRENTUSER)
            UserDefaults.standard.synchronize()
        }
    }
}

func fetchCurrentUserFromFirestore(userId: String, completion: @escaping (_ user: User?)->Void) {
    reference(.User).document(userId).getDocument { (snapshot, error) in
        guard let snapshot = snapshot else {  return }
        if snapshot.exists {
            let user = User(_dictionary: snapshot.data()! as NSDictionary)
            completion(user)
        } else {
            completion(nil)
        }
    }
}


func userDictionaryFrom(user: User) -> NSDictionary {
    let createdAt = dateFormatter().string(from: user.createdAt)
    let updatedAt = dateFormatter().string(from: user.updatedAt)
    
    return NSDictionary(objects: [user.objectId,  createdAt, updatedAt, user.email, user.loginMethod,
                                  user.pushId!, user.firstname, user.lastname, user.fullname, user.avatar,
                                  user.contacts, user.blockedUsers, user.isOnline, user.phoneNumber,
                                  user.countryCode, user.city, user.country],
                        forKeys: [OBJECTID as NSCopying, CREATEDAT as NSCopying, UPDATEDAT as NSCopying,
                                  EMAIL as NSCopying, LOGINMETHOD as NSCopying, PUSHID as NSCopying,
                                  FIRSTNAME as NSCopying, LASTNAME as NSCopying, FULLNAME as NSCopying,
                                  AVATAR as NSCopying, CONTACT as NSCopying, BLOCKEDUSERID as NSCopying,
                                  ISONLINE as NSCopying, PHONE as NSCopying, COUNTRYCODE as NSCopying,
                                  CITY as NSCopying, COUNTRY as NSCopying])
}

func getUsersFromFirestore(withIds: [String], completion: @escaping (_ usersArray: [User]) -> Void) {
    var count = 0
    var usersArray: [User] = []
    
    for userId in withIds {
        reference(.User).document(userId).getDocument { (snapshot, error) in
            guard let snapshot = snapshot else {  return }
            if snapshot.exists {
                let user = User(_dictionary: snapshot.data()! as NSDictionary)
                count += 1
                if user.objectId != User.currentId() {
                    usersArray.append(user)
                }
            } else {
                completion(usersArray)
            }
            if count == withIds.count {
                completion(usersArray)
            }
        }
    }
}

func updateCurrentUserInFirestore(withValues : [String : Any], completion: @escaping (_ error: Error?) -> Void) {
    if let dictionary = UserDefaults.standard.object(forKey: CURRENTUSER) {
        var tempWithValues = withValues
        let currentUserId = User.currentId()
        let updatedAt = dateFormatter().string(from: Date())
        tempWithValues[UPDATEDAT] = updatedAt
        
        let userObject = (dictionary as! NSDictionary).mutableCopy() as! NSMutableDictionary
        userObject.setValuesForKeys(tempWithValues)
        reference(.User).document(currentUserId).updateData(withValues) { (error) in
            if error != nil {
                completion(error)
                return
            }

            UserDefaults.standard.setValue(userObject, forKeyPath: CURRENTUSER)
            UserDefaults.standard.synchronize()
            completion(error)
        }
    }
}

func updateOneSignalId() {
    if User.currentUser() != nil {
        if let pushId = UserDefaults.standard.string(forKey: PUSHID) {
            setOneSignalId(pushId: pushId)
        } else {
            removeOneSignalId()
        }
    }
}

func setOneSignalId(pushId: String) {
    updateCurrentUserOneSignalId(newId: pushId)
}

func removeOneSignalId() {
    updateCurrentUserOneSignalId(newId: "")
}

func updateCurrentUserOneSignalId(newId: String) {
    updateCurrentUserInFirestore(withValues: [PUSHID : newId]) { (error) in
        if error != nil {
            print("error updating push id \(error!.localizedDescription)")
        }
    }
}

func checkBlockedStatus(withUser: User) -> Bool {
    return withUser.blockedUsers.contains(User.currentId())
}
