//
//  ChatCell.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/13/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit

class ChatCell: UITableViewCell {

    @IBOutlet weak var lblMessagesAmount: AppMediumLabel!
    @IBOutlet weak var lblMessage: AppMediumLabel!
    @IBOutlet weak var lblDate: AppMediumLabel!
    @IBOutlet weak var lblName: AppMediumLabel!
    @IBOutlet weak var ivAvatar: UIImageView!
    @IBOutlet weak var viewBadge: CircleImageView!
    
    var indexPath:IndexPath!
    var userCellDelegate:UserCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(clickAvatar))
        ivAvatar.isUserInteractionEnabled = true
        ivAvatar.addGestureRecognizer(tap)
    }
    
    @objc func clickAvatar() {
        userCellDelegate?.didClickAvatar(indexPath: indexPath)
    }
    
    func generateUserCell(chat:NSDictionary, indexPath: IndexPath) {
        self.indexPath = indexPath
        let decryptedText = CryptoTextHelper.decryptText(chatRoomId: chat[CHATROOMID] as! String, encryptedMessage: chat[LASTMESSAGE] as! String)
//        (chatRoomId: chat[CHATROOMID] as! String, message: chat[LASTMESSAGE] as! String)
        lblName.text = chat[WITHUSERNAME] as? String
        lblMessage.text = decryptedText
        if let count = chat[COUNTER] as? Int{
            lblMessagesAmount.text = String(count)
        }
        print("chat \(chat)")
        if let avatar = chat[AVATAR] as? String{
            print("AVATAR \(avatar)")
            imageFromData(pictureData: avatar) { (image) in
                if image != nil{
                    ivAvatar.image = image?.circleMasked
                }
            }
        }
        if chat[COUNTER] as? Int ?? 0 > 0{
            viewBadge.isHidden = false
            lblMessagesAmount.isHidden = false
        }else{
            lblMessagesAmount.isHidden = true
            viewBadge.isHidden = true
        }
        
        let date:Date!
        if let dateCreated = chat[DATE]{
            if (dateCreated as! String).count == 0{
                date = Date()
            }else{
                date = dateFormatter().date(from: dateCreated as! String)
            }
        }else{
            date = Date()
        }
        
        lblDate.text = timeElapsed(date: date)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
