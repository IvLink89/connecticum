//
//  PicturesVC.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/19/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit
import IDMPhotoBrowser

class PicturesVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var allImages: [UIImage] = []
    var allImageLinks: [String] = []

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "pictures_title".localized()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        print("pictures_title \(allImageLinks)")
        if allImageLinks.count > 0 {
            downloadImages()
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PictureCell", for: indexPath) as? PictureCell {
            cell.generateCell(image: allImages[indexPath.row])
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let photos = IDMPhoto.photos(withImages: allImages)
        let browser = IDMPhotoBrowser(photos: photos)
        browser?.displayDoneButton = false
        browser?.setInitialPageIndex(UInt(indexPath.row))
        self.present(browser!, animated: true, completion: nil)
    }
    
    func downloadImages() {
        for imageLink in allImageLinks {
            downloadImage(imageUrl: imageLink) { (image) in
                if image != nil {
                    self.allImages.append(image!)
                    self.collectionView.reloadData()
                }
            }
        }
    }

}
