//
//  Group.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/18/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import Foundation
import FirebaseFirestore

class Group {
    
    let groupDictionary: NSMutableDictionary
    
    init(groupId: String, subject: String, ownerId: String, members: [String], avatar: String) {
        groupDictionary = NSMutableDictionary(objects: [groupId, subject, ownerId, members, members, avatar], forKeys: [GROUPID as NSCopying, NAME as NSCopying, OWNERID as NSCopying, MEMBERS as NSCopying, MEMBERSTOPUSH as NSCopying, AVATAR as NSCopying])
    }
    
    func saveGroup() {
        let date = dateFormatter().string(from: Date())
        groupDictionary[DATE] = date
        reference(.Group).document(groupDictionary[GROUPID] as! String).setData(groupDictionary as! [String:Any])
    }
    
    class func updateGroup(groupId: String, withValues: [String:Any]) {
        reference(.Group).document(groupId).updateData(withValues)
    }
}
