//
//  IncomingMessage.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/16/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import Foundation
import JSQMessagesViewController

class IncomingMessage {
    
    var collectionView: JSQMessagesCollectionView
    
    init(collectionView: JSQMessagesCollectionView) {
        self.collectionView = collectionView
    }
    
    func createMessage(messageDictionary: NSDictionary, chatRoomId: String) -> JSQMessage? {
        var message: JSQMessage?
        let type = messageDictionary[TYPE] as! String
        
        switch type {
        case TEXT:
            message = createTextMessage(messageDictionary: messageDictionary, chatRoomId: chatRoomId)
        case PICTURE:
            message = createPictureMessage(messageDictionary: messageDictionary)
        case VIDEO:
            message = createVideoMessage(messageDictionary: messageDictionary)
        case AUDIO:
            message = createAudioMessage(messageDictionary: messageDictionary)
        case LOCATION:
            message = createLocationMessage(messageDictionary: messageDictionary)
        default:
            print("Unknown type")
        }
        
        if message != nil {
            return message
        }
        return nil
    }
    
    
    func createTextMessage(messageDictionary: NSDictionary, chatRoomId: String) -> JSQMessage {
        let name = messageDictionary[SENDERNAME] as? String
        let userId = messageDictionary[SENDERID] as? String
        
        var date: Date!
        
        if let created = messageDictionary[DATE] {
            if (created as! String).count == 0 {
                date = Date()
            } else {
                date = dateFormatter().date(from: created as! String)
            }
        } else {
            date = Date()
        }
        
        let decryptedText = CryptoTextHelper.decryptText(chatRoomId: chatRoomId, encryptedMessage: messageDictionary[MESSAGE] as! String)
        return JSQMessage(senderId: userId, senderDisplayName: name, date: date, text: decryptedText)
    }
    
    func createPictureMessage(messageDictionary: NSDictionary) -> JSQMessage {
        let name = messageDictionary[SENDERNAME] as? String
        let userId = messageDictionary[SENDERID] as? String
        var date: Date!
        
        if let created = messageDictionary[DATE] {
            if (created as! String).count != 14 {
                date = Date()
            } else {
                date = dateFormatter().date(from: created as! String)
            }
        } else {
            date = Date()
        }
        let mediaItem = ChatPhotoItem(image: nil)
        mediaItem?.appliesMediaViewMaskAsOutgoing = returnOutgoingStatusForUser(senderId: userId!)

        downloadImage(imageUrl: messageDictionary[PICTURE] as! String) { (image) in
            if image != nil {
                mediaItem?.image = image!
                self.collectionView.reloadData()
            }
        }
        
        return JSQMessage(senderId: userId, senderDisplayName: name, date: date, media: mediaItem)
    }
    
    func createVideoMessage(messageDictionary: NSDictionary) -> JSQMessage {
        let name = messageDictionary[SENDERNAME] as? String
        let userId = messageDictionary[SENDERID] as? String
        var date: Date!
        
        if let created = messageDictionary[DATE] {
            if (created as! String).count != 14 {
                date = Date()
            } else {
                date = dateFormatter().date(from: created as! String)
            }
        } else {
            date = Date()
        }
        let videoURL = NSURL(fileURLWithPath: messageDictionary[VIDEO] as! String)
        let mediaItem = ChatVideoItem(withFileURL: videoURL, maskOutgoing: returnOutgoingStatusForUser(senderId: userId!))
        
        downloadVideo(videoUrl: messageDictionary[VIDEO] as! String) { (isReadyToPlay, fileName) in
            let url = NSURL(fileURLWithPath: fileInDocumentsDirectory(fileName: fileName))
            mediaItem.status = SUCCESS
            mediaItem.fileURL = url
            imageFromData(pictureData: messageDictionary[PICTURE] as! String, withBlock: { (image) in
                if image != nil {
                    mediaItem.image = image!
                    self.collectionView.reloadData()
                }
            })
            self.collectionView.reloadData()
        }
        return JSQMessage(senderId: userId, senderDisplayName: name, date: date, media: mediaItem)
    }
    
    func createAudioMessage(messageDictionary: NSDictionary) -> JSQMessage {
        let name = messageDictionary[SENDERNAME] as? String
        let userId = messageDictionary[SENDERID] as? String

        let audioItem = JSQAudioMediaItem(data: nil)
        audioItem.appliesMediaViewMaskAsOutgoing = returnOutgoingStatusForUser(senderId: userId!)
        let audioMessage = JSQMessage(senderId: userId!, displayName: name!, media: audioItem)
        
        downloadAudio(audioUrl: messageDictionary[AUDIO] as! String) { (fileName) in
            let url = NSURL(fileURLWithPath: fileInDocumentsDirectory(fileName: fileName))
            let audioData = try? Data(contentsOf: url as URL)
            audioItem.audioData = audioData
            self.collectionView.reloadData()
        }
        return audioMessage!
    }
    
    
    func createLocationMessage(messageDictionary: NSDictionary) -> JSQMessage {
        let name = messageDictionary[SENDERNAME] as? String
        let userId = messageDictionary[SENDERID] as? String
        var date: Date!
        
        if let created = messageDictionary[DATE] {
            if (created as! String).count != 14 {
                date = Date()
            } else {
                date = dateFormatter().date(from: created as! String)
            }
        } else {
            date = Date()
        }
        
        let latitude = messageDictionary[LATITUDE] as? Double
        let longitude = messageDictionary[LONGITUDE] as? Double
        let mediaItem = JSQLocationMediaItem(location: nil)
        mediaItem?.appliesMediaViewMaskAsOutgoing = returnOutgoingStatusForUser(senderId: userId!)
        
        let location = CLLocation(latitude: latitude!, longitude: longitude!)
        mediaItem?.setLocation(location, withCompletionHandler: {
            self.collectionView.reloadData()
        })
        return JSQMessage(senderId: userId, senderDisplayName: name, date: date, media: mediaItem)
    }
    
    func returnOutgoingStatusForUser(senderId: String) -> Bool {
        return senderId == User.currentId()
    }
}
