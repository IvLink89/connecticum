//
//  InviteVC.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/21/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit
import Firebase
import ProgressHUD

class InviteVC: UITableViewController, UserCellDelegate{

    @IBOutlet weak var viewHeader: UIView!
    
    var users:[User]=[]
    var usersGroupped = NSDictionary() as! [String:[User]]
    var sectionTitlesList:[String] = []
    var newMembersIds:[String]=[]
    var currMembersIds:[String]=[]
    var group:NSDictionary!
    
    override func viewWillAppear(_ animated: Bool) {
        loadUsers(filter: "All")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        ProgressHUD.dismiss()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.tableFooterView = UIView()
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(image: UIImage(named: "save"), style: .plain, target: self, action: #selector(self.onSaveClicked))]
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        currMembersIds = group[MEMBERS] as! [String]
    }

    @IBAction func onTypeChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            loadUsers(filter: CITY)
        case 1:
            loadUsers(filter: COUNTRY)
        case 2:
            loadUsers(filter: "")
        default:
            return
        }
    }
    
    @objc func onSaveClicked(_ sender: Any) {
        updateGroup(group: group)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.usersGroupped.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionTitle = self.sectionTitlesList[section]
        let users = self.usersGroupped[sectionTitle]
        return users!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserCell
        let sectionTitle = self.sectionTitlesList[indexPath.section]
        let users = self.usersGroupped[sectionTitle]
        cell.generateUserCell(user: users![indexPath.row], indexPath: indexPath)
        cell.userCellDelegate = self
        return cell
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitlesList[section]
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return self.sectionTitlesList
    }
    
    override func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return index
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let sectionTitle = self.sectionTitlesList[indexPath.section]
        let users = self.usersGroupped[sectionTitle]
        let selectedUser = users![indexPath.row]
        
        if currMembersIds.contains(selectedUser.objectId) {
            ProgressHUD.showError("Already in the group!")
            return
        }
        if let cell = tableView.cellForRow(at: indexPath) {
            if cell.accessoryType == .checkmark {
                cell.accessoryType = .none
            } else {
                cell.accessoryType = .checkmark
            }
        }
        
        let selected = newMembersIds.contains(selectedUser.objectId)
        if selected {
            let objectIndex = newMembersIds.firstIndex(of: selectedUser.objectId)!
            newMembersIds.remove(at: objectIndex)
        } else {
            newMembersIds.append(selectedUser.objectId)
        }
        self.navigationItem.rightBarButtonItem?.isEnabled = newMembersIds.count > 0
    }
    
    func loadUsers(filter: String) {
        ProgressHUD.show()
        var query: Query!
        switch filter {
        case CITY:
            query = reference(.User).whereField(CITY, isEqualTo: User.currentUser()!.city).order(by: FIRSTNAME, descending: false)
        case COUNTRY:
            query = reference(.User).whereField(COUNTRY, isEqualTo: User.currentUser()!.country).order(by: FIRSTNAME, descending: false)
        default:
            query = reference(.User).order(by: FIRSTNAME, descending: false)
        }
        
        query.getDocuments { (snapshot, error) in
            self.users = []
            self.sectionTitlesList = []
            self.usersGroupped = [:]
            
            if error != nil {
                print(error!.localizedDescription)
                ProgressHUD.dismiss()
                self.tableView.reloadData()
                return
            }
            guard let snapshot = snapshot else {
                ProgressHUD.dismiss(); return
            }
            
            if !snapshot.isEmpty {
                for userDictionary in snapshot.documents {
                    let userDictionary = userDictionary.data() as NSDictionary
                    let fUser = User(_dictionary: userDictionary)
                    if fUser.objectId != User.currentId() {
                        self.users.append(fUser)
                    }
                }
                self.splitDataIntoSection()
                self.tableView.reloadData()
            }
            self.tableView.reloadData()
            ProgressHUD.dismiss()
        }
    }
    
    func didClickAvatar(indexPath: IndexPath) {
        let profileVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        let sectionTitle = self.sectionTitlesList[indexPath.section]
        let users = self.usersGroupped[sectionTitle]
        profileVC.user = users![indexPath.row]
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    func updateGroup(group: NSDictionary) {
        let tempMembers = currMembersIds + newMembersIds
        let tempMembersToPush = group[MEMBERSTOPUSH] as! [String] + newMembersIds
        let withValues = [MEMBERS : tempMembers, MEMBERSTOPUSH : tempMembersToPush]
        Group.updateGroup(groupId: group[GROUPID] as! String, withValues: withValues)
        createChatsForNewMembers(groupId: group[GROUPID] as! String, groupName: group[NAME] as! String, membersToPush: tempMembersToPush, avatar: group[AVATAR] as! String)
        updateExistingChatWithNewValues(chatRoomId: group[GROUPID] as! String, members: tempMembers, withValues: withValues)
        goToGroupChat(membersToPush: tempMembersToPush, members: tempMembers)
    }
    
    func goToGroupChat(membersToPush: [String], members: [String]) {
        let chatVC = ChatConversationVC()
        chatVC.titleName = group[NAME] as? String
        chatVC.membersIds = members
        chatVC.membersToPush = membersToPush
        chatVC.chatRoomId = group[GROUPID] as? String
        chatVC.isGroup = true
        chatVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(chatVC, animated: true)
    }
    
    fileprivate func splitDataIntoSection() {
        var sectionTitle: String = ""
        for i in 0..<self.users.count {
            let currentUser = self.users[i]
            let firstChar = currentUser.firstname.first!
            let firstCarString = "\(firstChar)"
            if firstCarString != sectionTitle {
                sectionTitle = firstCarString
                self.usersGroupped[sectionTitle] = []
                if !sectionTitlesList.contains(sectionTitle) {
                    self.sectionTitlesList.append(sectionTitle)
                }
            }
            self.usersGroupped[firstCarString]?.append(currentUser)
        }
    }
}
