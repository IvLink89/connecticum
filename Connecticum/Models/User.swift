//
//  User.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/4/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseFirestore

class User {
    
    let objectId: String
    var pushId: String?
    
    let createdAt: Date
    var updatedAt: Date
    
    var email: String
    var firstname: String
    var lastname: String
    var fullname: String
    var avatar: String
    var isOnline: Bool
    var phoneNumber: String
    var countryCode: String
    var country:String
    var city: String
    
    var contacts: [String]
    var blockedUsers: [String]
    let loginMethod: String
    
    //MARK: Initializers
    
    init(_objectId: String, _pushId: String?, _createdAt: Date,
         _updatedAt: Date, _email: String, _firstname: String,
         _lastname: String, _avatar: String = "", _loginMethod: String,
         _phoneNumber: String, _city: String, _country: String) {
        
        objectId = _objectId
        pushId = _pushId
        
        createdAt = _createdAt
        updatedAt = _updatedAt
        
        email = _email
        firstname = _firstname
        lastname = _lastname
        fullname = _firstname + " " + _lastname
        avatar = _avatar
        isOnline = true
        
        city = _city
        country = _country
        
        loginMethod = _loginMethod
        phoneNumber = _phoneNumber
        countryCode = ""
        blockedUsers = []
        contacts = []
        
    }
    
    init(_dictionary: NSDictionary) {
        
        objectId = _dictionary[OBJECTID] as! String
        pushId = _dictionary[PUSHID] as? String
        
        if let created = _dictionary[CREATEDAT] {
            if (created as! String).count != 14 {
                createdAt = Date()
            } else {
                createdAt = dateFormatter().date(from: created as! String)!
            }
        } else {
            createdAt = Date()
        }
        if let updateded = _dictionary[UPDATEDAT] {
            if (updateded as! String).count != 14 {
                updatedAt = Date()
            } else {
                updatedAt = dateFormatter().date(from: updateded as! String)!
            }
        } else {
            updatedAt = Date()
        }
        
        if let mail = _dictionary[EMAIL] {
            email = mail as! String
        } else {
            email = ""
        }
        if let fname = _dictionary[FIRSTNAME] {
            firstname = fname as! String
        } else {
            firstname = ""
        }
        if let lname = _dictionary[LASTNAME] {
            lastname = lname as! String
        } else {
            lastname = ""
        }
        fullname = firstname + " " + lastname
        if let avat = _dictionary[AVATAR] {
            avatar = avat as! String
        } else {
            avatar = ""
        }
        if let onl = _dictionary[ISONLINE] {
            isOnline = onl as! Bool
        } else {
            isOnline = false
        }
        if let phone = _dictionary[PHONE] {
            phoneNumber = phone as! String
        } else {
            phoneNumber = ""
        }
        if let countryC = _dictionary[COUNTRYCODE] {
            countryCode = countryC as! String
        } else {
            countryCode = ""
        }
        if let cont = _dictionary[CONTACT] {
            contacts = cont as! [String]
        } else {
            contacts = []
        }
        if let block = _dictionary[BLOCKEDUSERID] {
            blockedUsers = block as! [String]
        } else {
            blockedUsers = []
        }
        
        if let lgm = _dictionary[LOGINMETHOD] {
            loginMethod = lgm as! String
        } else {
            loginMethod = ""
        }
        if let cit = _dictionary[CITY] {
            city = cit as! String
        } else {
            city = ""
        }
        if let count = _dictionary[COUNTRY] {
            country = count as! String
        } else {
            country = ""
        }
    }
    
    class func currentId() -> String {
        return Auth.auth().currentUser?.uid ?? "0"
    }
    
    class func currentUser () -> User? {
        if Auth.auth().currentUser != nil {
            if let dictionary = UserDefaults.standard.object(forKey: CURRENTUSER) {
                return User.init(_dictionary: dictionary as! NSDictionary)
            }
        }
        return nil
    }

    class func loginUserWith(email: String, password: String, completion: @escaping (_ error: Error?) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password, completion: { (result, error) in
            if error != nil {
                completion(error)
                return
            } else {
                fetchCurrentUserFromFirestore(userId: result!.user.uid)
                completion(error)
            }
        })
    }
    
    class func registerUserWith(email: String, password: String, firstName: String,
                                lastName: String, avatar: String = "",
                                completion: @escaping (_ error: Error?) -> Void ) {
        Auth.auth().createUser(withEmail: email, password: password, completion: { (result, error) in
            if error != nil {
                completion(error)
                return
            }
            
            let user = User(_objectId: result!.user.uid, _pushId: "", _createdAt: Date(),
                    _updatedAt: Date(), _email: result!.user.email!,
                    _firstname: firstName, _lastname: lastName, _avatar: avatar,
                    _loginMethod: EMAIL, _phoneNumber: "", _city: "", _country: "")
            
            saveUserLocally(user: user)
            saveUserToFirestore(user: user)
            completion(error)
        })
    }
    
    class func registerUserWith(phoneNumber: String, verificationCode: String, verificationId: String!,
                                completion: @escaping (_ error: Error?, _ shouldLogin: Bool) -> Void) {
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationId, verificationCode: verificationCode)
        Auth.auth().signInAndRetrieveData(with: credential) { (result, error) in
            if error != nil {
                completion(error!, false)
                return
            }
            
            fetchCurrentUserFromFirestore(userId: result!.user.uid, completion: { (user) in
                if user != nil && user!.firstname != "" {
                    saveUserLocally(user: user!)
                    saveUserToFirestore(user: user!)
                    completion(error, true)
                } else {
                    let user = User(_objectId: result!.user.uid, _pushId: "", _createdAt: Date(),
                            _updatedAt: Date(), _email: "", _firstname: "", _lastname: "",
                            _avatar: "", _loginMethod: PHONE,
                            _phoneNumber: result!.user.phoneNumber!, _city: "", _country: "")
                    saveUserLocally(user: user)
                    saveUserToFirestore(user: user)
                    completion(error, false)
                }
            })
        }
    }
    
    class func logOutCurrentUser(completion: @escaping (_ success: Bool) -> Void) {
        userDefaults.removeObject(forKey: PUSHID)
        Connecticum.removeOneSignalId()
        
        userDefaults.removeObject(forKey: CURRENTUSER)
        userDefaults.synchronize()
        
        do {
            try Auth.auth().signOut()
            completion(true)
        } catch let error as NSError {
            completion(false)
            print(error.localizedDescription)
        }
    }
    
    class func deleteUser(completion: @escaping (_ error: Error?) -> Void) {
        let user = Auth.auth().currentUser
        user?.delete(completion: { (error) in
            completion(error)
        })
    }
}
