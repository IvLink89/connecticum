//
//  CircleImageView.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/6/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit

@IBDesignable
class CircleImageView: UIImageView {

    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        initView()
    }

    override func awakeFromNib() {
        initView()
    }
    
    func initView(){
        layer.cornerRadius = frame.width/2
        clipsToBounds = true
    }
}
