//
//  AppTextField.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/2/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit

class AppTextField: UITextField {
    
    override func prepareForInterfaceBuilder() {
        initView()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }
    
    func initView() {
        backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.8991598887)
        layer.cornerRadius = 5.0
        layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        layer.borderWidth = 1
        textAlignment = .left
        textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        font = UIFont(name: "Avenir-Light", size: 18)
        
        if let wrapPlaceholder = placeholder{
            let color = NSAttributedString.Key.foregroundColor
            let place = NSAttributedString(string: wrapPlaceholder.localized(), attributes: [color:#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)])
            attributedPlaceholder = place
        }
//        autocapitalizationType = UITextAutocapitalizationType.sentences
        autocorrectionType = UITextAutocorrectionType.yes
    }
}
