//
//  ChatHelper.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/11/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import Foundation

func startPrivateChat(owner:User, user:User) -> String{
    let ownerId = owner.objectId
    let userId = user.objectId
    var chatRoomId = ""
    
    let value = ownerId.compare(userId).rawValue
    if value<0{
        chatRoomId = ownerId+userId
    }else{
        chatRoomId = userId+ownerId
    }
    
    let members = [ownerId,userId]
    createChat(members: members, chatRoomId: chatRoomId, withUserName: "", type: PRIVATE, users: [owner, user], avatarOfGroup: nil)
    return chatRoomId
}

func createChat(members: [String], chatRoomId: String, withUserName: String, type:String, users: [User]?, avatarOfGroup: String?){
    var tempMembers = members
    reference(.Chat).whereField(CHATROOMID, isEqualTo: chatRoomId).getDocuments { (snapshot, error) in
        guard let snapshot = snapshot else{return}
        if !snapshot.isEmpty{
            for doc in snapshot.documents{
                let data = doc.data() as NSDictionary
                if let currUserId = data[USERID]{
                    if tempMembers.contains(currUserId as! String){
                        tempMembers.remove(at: tempMembers.firstIndex(of: currUserId as! String)!)
                    }
                }
            }
        }
        for userId in tempMembers{
            createItem(userId: userId, members: members, chatRoomId: chatRoomId, withUserName: withUserName, type: type, users: users, avatarOfGroup: avatarOfGroup)
        }
    }
}

func createItem(userId: String, members: [String], chatRoomId: String, withUserName: String, type:String, users: [User]?, avatarOfGroup: String?) {
    let document = reference(.Chat).document()
    let docId = document.documentID
    let date = dateFormatter().string(from: Date())
    var recent:[String:Any]!
    
    if type == PRIVATE{
        var user:User?
        if users != nil && users!.count > 0{
            if userId == User.currentId(){
                user = users!.last
            }else{
                user = users!.first
            }
        }
        recent = [CHAT_ID:docId, USERID:userId, CHATROOMID:chatRoomId, MEMBERS: members, MEMBERSTOPUSH: members, WITHUSERNAME:user!.fullname, WITHUSERID:user!.objectId, LASTMESSAGE:"", COUNTER: 0, DATE: date, TYPE: type, AVATAR: avatarOfGroup] as [String:Any]
    }else{
        if avatarOfGroup != nil{
            recent = [CHAT_ID:docId, USERID:userId, CHATROOMID:chatRoomId, MEMBERS: members, MEMBERSTOPUSH: members, WITHUSERNAME:withUserName, LASTMESSAGE:"", COUNTER: 0, DATE: date, TYPE: type, AVATAR: avatarOfGroup] as [String:Any]
        }
    }
    
    document.setData(recent)
}

func removeChat(chatDict: NSDictionary)  {
    if let chatId = chatDict[CHAT_ID]{
        reference(.Chat).document(chatId as! String).delete()
    }
}

func restartChat(chat:NSDictionary) {
    if chat[TYPE] as! String == PRIVATE{
        createChat(members: chat[MEMBERSTOPUSH] as! [String], chatRoomId: chat[CHATROOMID] as! String, withUserName: chat[WITHUSERNAME] as! String, type: PRIVATE, users: [User.currentUser()!], avatarOfGroup: nil)
    }
    if chat[TYPE] as! String == GROUP{
        createChat(members: chat[MEMBERSTOPUSH] as! [String], chatRoomId: chat[CHATROOMID] as! String, withUserName: chat[WITHUSERNAME] as! String, type: GROUP, users: nil, avatarOfGroup: chat[AVATAR] as? String)
    }
}

func updateChats(chatRoomId: String, lastMessage: String) {
    reference(.Chat).whereField(CHATROOMID, isEqualTo: chatRoomId).getDocuments { (snapshot, error) in
        guard let snapshot = snapshot else { return }
        if !snapshot.isEmpty {
            for recent in snapshot.documents {
                let current = recent.data() as NSDictionary
                updateChatItem(chat: current, lastMessage: lastMessage)
            }
        }
    }
}

func updateChatItem(chat: NSDictionary, lastMessage: String) {
    let date = dateFormatter().string(from: Date())
    var counter = chat[COUNTER] as! Int
    if chat[USERID] as? String != User.currentId() {
        counter += 1
    }
    let values = [LASTMESSAGE : lastMessage, COUNTER : counter, DATE : date] as [String : Any]
    reference(.Chat).document(chat[CHAT_ID] as! String).updateData(values)
}

func clearChatCounter(chatRoomId: String) {
    reference(.Chat).whereField(CHATROOMID, isEqualTo: chatRoomId).getDocuments { (snapshot, error) in
        guard let snapshot = snapshot else { return }
        if !snapshot.isEmpty {
            for recent in snapshot.documents {
                let current = recent.data() as NSDictionary
                if current[USERID] as? String == User.currentId() {
                    clearChatCounterItem(chat: current)
                }
            }
        }
    }
}

func clearChatCounterItem(chat: NSDictionary) {
    reference(.Chat).document(chat[CHAT_ID] as! String).updateData([COUNTER : 0])
}

func startGroupChat(group: Group) {
    let chatRoomId = group.groupDictionary[GROUPID] as! String
    let members = group.groupDictionary[MEMBERS] as! [String]
    createChat(members: members, chatRoomId: chatRoomId, withUserName: group.groupDictionary[NAME] as! String, type: GROUP, users: nil, avatarOfGroup: group.groupDictionary[AVATAR] as? String)
}

func createChatsForNewMembers(groupId: String, groupName: String, membersToPush: [String], avatar: String) {
    createChat(members: membersToPush, chatRoomId: groupId, withUserName: groupName, type: GROUP, users: nil, avatarOfGroup: avatar)
}

func updateExistingChatWithNewValues(chatRoomId: String, members: [String], withValues: [String : Any]) {
    reference(.Chat).whereField(CHATROOMID, isEqualTo: chatRoomId).getDocuments { (snapshot, error) in
        guard let snapshot = snapshot else { return }
        if !snapshot.isEmpty {
            for chat in snapshot.documents {
                let current = chat.data() as NSDictionary
                updateChat(chatId: current[CHAT_ID] as! String, withValues: withValues)
            }
        }
    }
}

func updateChat(chatId: String, withValues: [String : Any]) {
    reference(.Chat).document(chatId).updateData(withValues)
}

func blockUser(userToBlock: User) {
    let userId1 = User.currentId()
    let userId2 = userToBlock.objectId
    var chatRoomId = ""
    let value = userId1.compare(userId2).rawValue
    if value < 0 {
        chatRoomId = userId1 + userId2
    } else {
        chatRoomId = userId2 + userId1
    }
    getChatsFor(chatRoomId: chatRoomId)
}

func getChatsFor(chatRoomId: String) {
    reference(.Chat).whereField(CHATROOMID, isEqualTo: chatRoomId).getDocuments { (snapshot, error) in
        guard let snapshot = snapshot else { return }
        if !snapshot.isEmpty {
            for doc in snapshot.documents {
                let chatDict = doc.data() as NSDictionary
                removeChat(chatDict: chatDict)
            }
        }
    }
}

