//
//  DateTimeUtils.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/5/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import Foundation

private let dateFormat = "yyyyMMddHHmmss"

func dateFormatter() -> DateFormatter {
    let dateFormatter = DateFormatter()
    dateFormatter.timeZone = TimeZone(secondsFromGMT: TimeZone.current.secondsFromGMT())
    dateFormatter.dateFormat = dateFormat
    return dateFormatter
}

func timeElapsed(date: Date) -> String {
    let seconds = NSDate().timeIntervalSince(date)
    var elapsed: String?
    
    if (seconds < 60) {
        elapsed = "utils_msg_right_now".localized()
    } else if (seconds < 60 * 60) {
        let minutes = Int(seconds / 60)
        let minText = String.localizedStringWithFormat("minute", minutes)
//        if minutes > 1 {
//            minText = "mins"
//        }
        elapsed = "\(minutes) \(minText)"
    } else if (seconds < 24 * 60 * 60) {
        let hours = Int(seconds / (60 * 60))
        let hourText = String.localizedStringWithFormat("hour", hours)
//        if hours > 1 {
//            hourText = "hours"
//        }
        elapsed = "\(hours) \(hourText)"
    } else {
        let currentDateFormater = dateFormatter()
        currentDateFormater.dateFormat = "dd/MM/YYYY"
        elapsed = "\(currentDateFormater.string(from: date))"
    }
    return elapsed!
}

func formatCallTime(date: Date) -> String {
    let seconds = NSDate().timeIntervalSince(date)
    var elapsed: String?
    
    if (seconds < 60) {
        elapsed = "utils_msg_right_now".localized()
    }  else if (seconds < 24 * 60 * 60) {
        let currentDateFormater = dateFormatter()
        currentDateFormater.dateFormat = "HH:mm"
        elapsed = "\(currentDateFormater.string(from: date))"
    } else {
        let currentDateFormater = dateFormatter()
        currentDateFormater.dateFormat = "dd/MM/YYYY"
        elapsed = "\(currentDateFormater.string(from: date))"
    }
    return elapsed!
}

func readTimeFrom(dateString: String) -> String {
    let date = dateFormatter().date(from: dateString)
    let currentDateFormat = dateFormatter()
    currentDateFormat.dateFormat = "HH:mm"
    return currentDateFormat.string(from: date!)
}

