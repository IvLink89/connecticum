//
//  ContactsVC.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/21/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit
import Contacts
import FirebaseFirestore
import ProgressHUD

class ContactsVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating, UserCellDelegate {

    var users: [User] = []
    var matchedUsers: [User] = []
    var filteredMatchedUsers: [User] = []
    var allUsersGrouped = NSDictionary() as! [String : [User]]
    var sectionTitleList: [String] = []
    
    var isGroup = false
    var memberIdsOfGroupChat: [String] = []
    var membersOfGroupChat: [User] = []
    
    let searchController = UISearchController(searchResultsController: nil)
    
    @IBOutlet weak var tableView: UITableView!
    
    lazy var contacts: [CNContact] = {
        let contactStore = CNContactStore()
        let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactEmailAddressesKey,
            CNContactPhoneNumbersKey,
            CNContactImageDataAvailableKey,
            CNContactThumbnailImageDataKey] as [Any]
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }
        
        var results: [CNContact] = []
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                results.append(contentsOf: containerResults)
            } catch {
                print("Error fetching results for container")
            }
        }
        return results
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        loadUsers()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Contacts"
        navigationItem.searchController = searchController
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        
        tableView.sectionIndexColor = #colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1)
        searchController.searchBar.tintColor = #colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1)
        
        setupButtons()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return 1
        } else {
            return self.allUsersGrouped.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredMatchedUsers.count
        } else {
            let sectionTitle = self.sectionTitleList[section]
            let users = self.allUsersGrouped[sectionTitle]
            return users!.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell") as! UserCell
        var user: User
        if searchController.isActive && searchController.searchBar.text != "" {
            user = filteredMatchedUsers[indexPath.row]
        } else {
            let sectionTitle = self.sectionTitleList[indexPath.section]
            let users = self.allUsersGrouped[sectionTitle]
            user = users![indexPath.row]
        }
        cell.userCellDelegate = self
        cell.generateUserCell(user: user, indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if searchController.isActive && searchController.searchBar.text != "" {
            return ""
        } else {
            return self.sectionTitleList[section]
        }
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if searchController.isActive && searchController.searchBar.text != "" {
            return nil
        } else {
            return self.sectionTitleList
        }
    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return index
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let headerView = view as! UITableViewHeaderFooterView
        headerView.textLabel?.font = UIFont(name: "Avenir-Medium", size: 16)!
        headerView.backgroundView?.backgroundColor = #colorLiteral(red: 0.8117647059, green: 0.8470588235, blue: 0.862745098, alpha: 1)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let sectionTitle = self.sectionTitleList[indexPath.section]
        let userToChat : User
        
        if searchController.isActive && searchController.searchBar.text != "" {
            userToChat = filteredMatchedUsers[indexPath.row]
        } else {
            let users = self.allUsersGrouped[sectionTitle]
            userToChat = users![indexPath.row]
        }
        
        if !isGroup {
            if !checkBlockedStatus(withUser: userToChat) {
                let chatVC = ChatConversationVC()
                chatVC.titleName = userToChat.firstname
                chatVC.membersIds = [User.currentId(), userToChat.objectId]
                chatVC.membersToPush = [User.currentId(), userToChat.objectId]
                chatVC.chatRoomId = startPrivateChat(owner: User.currentUser()!, user: userToChat)
                chatVC.isGroup = false
                chatVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(chatVC, animated: true)
            } else {
                ProgressHUD.showError("This user is not available for chat")
            }
        } else {
            if let cell = tableView.cellForRow(at: indexPath) {
                if cell.accessoryType == .checkmark {
                    cell.accessoryType = .none
                } else {
                    cell.accessoryType = .checkmark
                }
            }
            let selected = memberIdsOfGroupChat.contains(userToChat.objectId)
            if selected {
                let objectIndex = memberIdsOfGroupChat.firstIndex(of: userToChat.objectId)
                memberIdsOfGroupChat.remove(at: objectIndex!)
                membersOfGroupChat.remove(at: objectIndex!)
            } else {
                memberIdsOfGroupChat.append(userToChat.objectId)
                membersOfGroupChat.append(userToChat)
            }
            self.navigationItem.rightBarButtonItem?.isEnabled = memberIdsOfGroupChat.count > 0
        }
    }
    
    @objc func shareClicked() {
        let text = "settings_cache_didnt_clean".localized() + " \(APPURL)"
        let objectsToShare:[Any] = [text]
        let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.setValue("settings_cache_didnt_clean".localized(), forKey: "subject")
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @objc func onUsersClicked() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "UsersVC") as? UsersVC {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @objc func onNewGroupClicked() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "NewGroupVC") as? NewGroupVC {
            controller.memberIds = memberIdsOfGroupChat
            controller.allMembers = membersOfGroupChat
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func loadUsers() {
        ProgressHUD.show()
        reference(.User).order(by: FIRSTNAME, descending: false).getDocuments { (snapshot, error) in
            guard let snapshot = snapshot else {
                ProgressHUD.dismiss()
                return
            }
            if !snapshot.isEmpty {
                self.matchedUsers = []
                self.users.removeAll()
                for userDictionary in snapshot.documents {
                    let userDictionary = userDictionary.data() as NSDictionary
                    let user = User(_dictionary: userDictionary)
                    if user.objectId != User.currentId() {
                        self.users.append(user)
                    }
                }
                ProgressHUD.dismiss()
                self.tableView.reloadData()
            }
            ProgressHUD.dismiss()
            self.compareUsers()
        }
    }
    
    func compareUsers() {
        for user in users {
            if user.phoneNumber != "" {
                let contact = searchForContactUsingPhoneNumber(phoneNumber: user.phoneNumber)
                if contact.count > 0 {
                    matchedUsers.append(user)
                }
                self.tableView.reloadData()
            }
        }
        self.splitDataInToSection()
    }
    
    func searchForContactUsingPhoneNumber(phoneNumber: String) -> [CNContact] {
        var result: [CNContact] = []
        for contact in self.contacts {
            if !contact.phoneNumbers.isEmpty {
                let phoneNumberToCompareAgainst = updatePhoneNumber(phoneNumber: phoneNumber, replacePlusSign: true)
                for phoneNumber in contact.phoneNumbers {
                    let fulMobNumVar  = phoneNumber.value
                    let countryCode = fulMobNumVar.value(forKey: "countryCode") as? String
                    let phoneNumber = fulMobNumVar.value(forKey: "digits") as? String
                    let contactNumber = removeCountryCode(countryCodeLetters: countryCode!, fullPhoneNumber: phoneNumber!)
                    if contactNumber == phoneNumberToCompareAgainst {
                        result.append(contact)
                    }
                }
            }
        }
        return result
    }
    
    func updatePhoneNumber(phoneNumber: String, replacePlusSign: Bool) -> String {
        if replacePlusSign {
            return phoneNumber.replacingOccurrences(of: "+", with: "").components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
        } else {
            return phoneNumber.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
        }
    }
    
    func removeCountryCode(countryCodeLetters: String, fullPhoneNumber: String) -> String {
        let countryCode = CountryCode()
        let countryCodeToRemove = countryCode.codeDictionaryShort[countryCodeLetters.uppercased()]
        let updatedCode = updatePhoneNumber(phoneNumber: countryCodeToRemove!, replacePlusSign: true)
        let replacedNUmber = fullPhoneNumber.replacingOccurrences(of: updatedCode, with: "").components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
        return replacedNUmber
    }
    
    func splitDataInToSection() {
        var sectionTitle: String = ""
        for i in 0..<self.matchedUsers.count {
            let currentUser = self.matchedUsers[i]
            let firstChar = currentUser.firstname.first!
            let firstCharString = "\(firstChar)"
            if firstCharString != sectionTitle {
                sectionTitle = firstCharString
                self.allUsersGrouped[sectionTitle] = []
                if !sectionTitleList.contains(sectionTitle) {
                    self.sectionTitleList.append(sectionTitle)
                }
            }
            self.allUsersGrouped[firstCharString]?.append(currentUser)
        }
        tableView.reloadData()
    }
    
    func filteredContentForSearchText(searchText: String, scope: String = "All") {
        filteredMatchedUsers = matchedUsers.filter({ (user) -> Bool in
            return user.firstname.lowercased().contains(searchText.lowercased())
        })
        tableView.reloadData()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredContentForSearchText(searchText: searchController.searchBar.text!)
    }
    
    func didClickAvatar(indexPath: IndexPath) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as? ProfileVC {
            var user: User!
            if searchController.isActive && searchController.searchBar.text != "" {
                user = filteredMatchedUsers[indexPath.row]
            } else {
                let sectionTitle = self.sectionTitleList[indexPath.row]
                let users = self.allUsersGrouped[sectionTitle]
                user = users![indexPath.row]
            }
            controller.user = user
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func setupButtons() {
        if isGroup {
            let nextButton = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(self.onNewGroupClicked))
            self.navigationItem.rightBarButtonItem = nextButton
            self.navigationItem.rightBarButtonItems!.first!.isEnabled = false
        } else {
            let inviteButton = UIBarButtonItem(image: UIImage(named: "invite"), style: .plain, target: self, action: #selector(self.shareClicked))
            let searchButton = UIBarButtonItem(image: UIImage(named: "nearMe"), style: .plain, target: self, action: #selector(self.onUsersClicked))
            self.navigationItem.rightBarButtonItems = [inviteButton, searchButton]
        }
    }

}
