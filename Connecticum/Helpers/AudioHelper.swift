//
//  AudioHelper.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/16/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import Foundation
import IQAudioRecorderController

class AudioHelper {
    
    var delegate: IQAudioRecorderViewControllerDelegate
    
    init(delegate_: IQAudioRecorderViewControllerDelegate) {
        delegate = delegate_
    }
    
    func presentAudioRecorder(target: UIViewController) {
        let controller = IQAudioRecorderViewController()
        controller.delegate = delegate
        controller.title = "audio_helper_title".localized()
        controller.maximumRecordDuration = AUDIOMAXDURATION
        controller.allowCropping = true
        target.presentBlurredAudioRecorderViewControllerAnimated(controller)
    }
}
