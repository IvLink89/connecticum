//
//  Badge.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/26/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import Foundation
import Firebase

func chatsBadgeCount(withBlock: @escaping(_ amount:Int) -> Void) {
    chatBadgeHandler = reference(.Chat).whereField(USERID, isEqualTo: User.currentId()).addSnapshotListener({ (snapshot, error) in
        var badge = 0
        var counter = 0
        guard let snapshot = snapshot else {return}
        
        if !snapshot.isEmpty{
            let chats = snapshot.documents
            for chat in chats{
                let currentChat = chat.data() as NSDictionary
                badge += currentChat[COUNTER] as! Int
                counter+=1
                if counter == chats.count{
                    withBlock(badge)
                }
            }
        }else{
            withBlock(badge)
        }
    })
}

func setBadges(controller: UITabBarController) {
    chatsBadgeCount { (badge) in
        if badge != 0{
            controller.tabBar.items![1].badgeValue = "\(badge)"
            controller.tabBar.items![1].badgeColor = #colorLiteral(red: 0.9568627451, green: 0.262745098, blue: 0.2117647059, alpha: 1)
        }else{
            controller.tabBar.items![1].badgeValue = nil
        }
    }
}
