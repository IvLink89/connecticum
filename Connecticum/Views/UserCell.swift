//
//  UserCell.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/8/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {
    
    @IBOutlet weak var ivAvatar: UIImageView!
    @IBOutlet weak var lblName: AppMediumLabel!
    var indexPath:IndexPath!
    var userCellDelegate:UserCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(clickAvatar))
        ivAvatar.isUserInteractionEnabled = true
        ivAvatar.addGestureRecognizer(tap)
    }
    
    @objc func clickAvatar() {
        userCellDelegate?.didClickAvatar(indexPath: indexPath)
    }
    
    func generateUserCell(user:User, indexPath: IndexPath) {
        self.indexPath = indexPath
        lblName.text = user.fullname
        if user.avatar != ""{
            imageFromData(pictureData: user.avatar) { (image) in
                if image != nil{
                    ivAvatar.image = image?.circleMasked
                }
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
