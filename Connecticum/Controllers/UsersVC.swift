//
//  UsersVC.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/8/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit
import Firebase
import ProgressHUD

class UsersVC: UITableViewController, UISearchResultsUpdating, UserCellDelegate{
    
    @IBOutlet var tableViewUsers: UITableView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var scFilter: UISegmentedControl!
    
    var users:[User]=[]
    var usersFiltered:[User]=[]
    var usersGroupped = NSDictionary() as! [String:[User]]
    var sectionTitlesList:[String] = []
    
    var searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if #available(iOS 11, *) {
//            UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)
//            UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .highlighted)
//        } else {
//            UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: 0, vertical: -60), for:UIBarMetrics.default)
//        }
        
        navigationItem.title = "users_title".localized()
        navigationItem.searchController = searchController
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        
        scFilter.setTitle("users_city".localized(), forSegmentAt: 0)
        scFilter.setTitle("users_country".localized(), forSegmentAt: 1)
        scFilter.setTitle("users_all".localized(), forSegmentAt: 2)
        
        tableViewUsers.sectionIndexColor = #colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1)
        searchController.searchBar.tintColor = #colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1)
        
        loadUsers(filter: "")
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        if searchController.isActive && searchController.searchBar.text != ""{
           return 1
        }
        return usersGroupped.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != ""{
            return usersFiltered.count
        }
        
        let sectionTitle = sectionTitlesList[section]
        let users = usersGroupped[sectionTitle]
        return users?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as? UserCell {
            let user = fetchUser(indexPath: indexPath)
            cell.generateUserCell(user: user, indexPath: indexPath)
            cell.userCellDelegate = self
            return cell
        }
        return UITableViewCell()
    }
 
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if searchController.isActive && searchController.searchBar.text != ""{
            return ""
        }else{
            return sectionTitlesList[section].uppercased()
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let headerView = view as! UITableViewHeaderFooterView
        headerView.textLabel?.font = UIFont(name: "Avenir-Medium", size: 16)!
        headerView.backgroundView?.backgroundColor = #colorLiteral(red: 0.8117647059, green: 0.8470588235, blue: 0.862745098, alpha: 1)
        
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if searchController.isActive && searchController.searchBar.text != ""{
            return nil
        }else{
            return sectionTitlesList
        }
    }
    
    override func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return index
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let user = fetchUser(indexPath: indexPath)
        
        if !checkBlockedStatus(withUser: user) {
            let chatVC = ChatConversationVC()
            chatVC.titleName = user.firstname
            chatVC.membersToPush = [User.currentId(), user.objectId]
            chatVC.membersIds = [User.currentId(), user.objectId]
            chatVC.chatRoomId = startPrivateChat(owner: User.currentUser()!, user: user)
            chatVC.isGroup = false
            chatVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(chatVC, animated: true)
        } else {
            ProgressHUD.showError("users_blocked_user_alert".localized())
        }
    }
    
    func fetchUser(indexPath:IndexPath) -> User {
        var user:User!
        if searchController.isActive && searchController.searchBar.text != ""{
            user = usersFiltered[indexPath.row]
        }else{
            let sectionTitle = sectionTitlesList[indexPath.section]
            let users = usersGroupped[sectionTitle]
            user = users?[indexPath.row]
        }
        return user
    }
    
    func didClickAvatar(indexPath: IndexPath) {
                if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as? ProfileVC {
                    controller.user = fetchUser(indexPath: indexPath)
                    navigationController?.pushViewController(controller, animated: true)
                }
    }
    
    @IBAction func filterChanged(_ sender: UISegmentedControl) {
        sectionTitlesList = []
        switch sender.selectedSegmentIndex {
        case 0:
            loadUsers(filter: CITY)
        case 1:
            loadUsers(filter: COUNTRY)
        case 2:
            loadUsers(filter: "")
        default:
           return
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContent(searchText: searchController.searchBar.text!)
    }

    func filterContent(searchText: String, scope: String = "All") {
        usersFiltered = users.filter({ (user) -> Bool in
            return user.fullname.lowercased().contains(searchText.lowercased())
        })
        tableViewUsers.reloadData()
    }
    
    func loadUsers(filter: String) {
        ProgressHUD.show()
        var query:Query!
        
        switch filter {
        case COUNTRY:
            query = reference(.User)
                .whereField(COUNTRY, isEqualTo: User.currentUser()?.country ?? "country")
                .order(by: FIRSTNAME, descending: false)
        case CITY:
            query = reference(.User)
                .whereField(CITY, isEqualTo: User.currentUser()?.city ?? "city")
                .order(by: FIRSTNAME, descending: false)
        default:
            query = reference(.User)
                .order(by: FIRSTNAME, descending: false)
        }
        query.getDocuments { (snapshot, error) in
            self.users = []
            self.usersFiltered = []
            self.usersGroupped = [:]
            if error != nil{
                self.tableViewUsers.reloadData()
                print(error?.localizedDescription ?? "None")
                ProgressHUD.showError(error?.localizedDescription)
                return
            }
            guard let snapshot = snapshot else {
                return
            }
            if !snapshot.isEmpty{
                for document in snapshot.documents{
                    let userDict = document.data() as NSDictionary
                    let user = User(_dictionary: userDict)
                    if user.objectId != User.currentId(){
                        self.users.append(user)
                    }
                }
                self.splitDataIntoSections()
            }
            self.tableViewUsers.reloadData()
            ProgressHUD.dismiss()
        }
    }
    
    func splitDataIntoSections(){
        var sectionTitle:String = ""
        for i in 0..<users.count{
            let currentUser = users[i]
            let firstChar = "\(currentUser.firstname.first ?? "-")"
            
            if firstChar != sectionTitle{
                sectionTitle = firstChar
                usersGroupped[sectionTitle] = []
                if !sectionTitlesList.contains(sectionTitle) {
                    self.sectionTitlesList.append(sectionTitle)
                }
            }
            usersGroupped[firstChar]?.append(currentUser)
        }
    }
}
