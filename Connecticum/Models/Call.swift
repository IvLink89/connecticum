//
//  Call.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/22/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import Foundation

class Call {
    
    var objectId: String?
    var callerId: String?
    var callerFullName: String?
    var opponentId: String?
    var opponentFullName: String?
    var status: String?
    var isIncoming: Bool?
    var callDate: Date?
    
    init(callerId:String, opponentId:String, callerName:String, opponentName:String) {
        objectId = UUID().uuidString
        self.callerId = callerId
        callerFullName = callerName
        self.opponentId = opponentId
        opponentFullName = opponentName
        status = ""
        isIncoming = false
        callDate = Date()
    }
    
    init(dict: NSDictionary) {
        objectId = dict[OBJECTID] as? String
        if let callId = dict[CALLERID] as? String{
            callerId = callId
        }else{
            callerId = ""
        }
        if let oppId = dict[WITHUSERID] as? String{
            opponentId = oppId
        }else{
            opponentId = ""
        }
        if let callerName = dict[CALLERFULLNAME] as? String{
            callerFullName = callerName
        }else{
            callerFullName = "call_unknown_call".localized()
        }
        if let oppName = dict[WITHUSERFULLNAME] as? String{
            opponentFullName = oppName
        }else{
            opponentFullName = "call_unknown_call".localized()
        }
        if let stat = dict[STATUS] as? String{
            status = stat
        }else{
            status = ""
        }
        if let income = dict[ISINCOMING] as? Bool{
            isIncoming = income
        }else{
            isIncoming = false
        }
        if let date = dict[DATE]{
            if (date as! String).count > 0{
                callDate = dateFormatter().date(from: date as! String)
            }else{
                callDate = Date()
            }
        }else{
            callDate = Date()
        }
    }
    
    func dictionaryFromCall() -> NSDictionary {
        let date = dateFormatter().string(from: callDate!)
        return NSDictionary(objects: [date, callerId!, callerFullName!, opponentId!, opponentFullName!, status!, isIncoming!, objectId!],
                            forKeys: [DATE as NSCopying,CALLERID as NSCopying, CALLERFULLNAME as NSCopying, WITHUSERID as NSCopying, WITHUSERFULLNAME as NSCopying, STATUS as NSCopying, ISINCOMING as NSCopying, OBJECTID as NSCopying])
    }
    
    func saveCallInBackground() {
        reference(.Call).document(callerId!).collection(callerId!).document(objectId!).setData(dictionaryFromCall() as! [String: Any])
        reference(.Call).document(opponentId!).collection(opponentId!).document(objectId!).setData(dictionaryFromCall() as! [String: Any])
    }
    
    func deleteCall() {
        reference(.Call).document(User.currentId()).collection(User.currentId()).document(objectId!).delete()
    }
}
