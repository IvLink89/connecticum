//
//  UserCellDelegate.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/13/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import Foundation

protocol UserCellDelegate {
    func didClickAvatar(indexPath:IndexPath)
}
