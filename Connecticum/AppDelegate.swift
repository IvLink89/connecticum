//
//  AppDelegate.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/2/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation
import OneSignal
import PushKit
import Sinch

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate, SINClientDelegate, SINCallClientDelegate, PKPushRegistryDelegate, SINManagedPushDelegate {
    
    var window: UIWindow?
    var authListener: AuthStateDidChangeListenerHandle?
    
    var locationManager: CLLocationManager?
    var coordinates: CLLocationCoordinate2D?
    
    var client: SINClient!
    var push: SINManagedPush!
    var callKitProvider: SINCallKitProvider!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        let db = Firestore.firestore()
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
        
        authListener = Auth.auth().addStateDidChangeListener({ (auth, user) in
            print("authListener \(user?.displayName ?? "None")")
            Auth.auth().removeStateDidChangeListener(self.authListener!)
            if user != nil {
                if UserDefaults.standard.object(forKey: CURRENTUSER) != nil {
                    print("DispatchQueue")
                    DispatchQueue.main.async {
                        self.makeTransitionToMain()
                    }
                }
            }
        })
        
        self.voipRegistration()
        
        self.push = Sinch.managedPush(with: .development)
        self.push.delegate = self
        self.push.setDesiredPushTypeAutomatically()
        
        func userDidLogin(userId: String) {
            self.push.registerUserNotificationSettings()
            self.initSinchWithUserId(userId: userId)
            self.startOneSignal()
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(USER_DID_LOGIN_NOTIFICATION), object: nil, queue: nil) { (note) in
            let userId = note.userInfo![USERID] as! String
            UserDefaults.standard.set(userId, forKey: USERID)
            UserDefaults.standard.synchronize()
            userDidLogin(userId: userId)
        }

        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .alert, .sound], completionHandler: { (granted, error) in
            })
            application.registerForRemoteNotifications()
        } else {
            let types: UIUserNotificationType = [.alert, .badge, .sound]
            let settings = UIUserNotificationSettings(types: types, categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        OneSignal.initWithLaunchOptions(launchOptions, appId: ONESIGNALAPPID, handleNotificationReceived: nil, handleNotificationAction: nil, settings: [kOSSettingsKeyInAppAlerts : false])
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        chatBadgeHandler?.remove()
        if User.currentUser() != nil {
            updateCurrentUserInFirestore(withValues: [ISONLINE : false]) { (success) in
                
            }
        }
        locationMangerStop()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        if callKitProvider != nil {
            let call = callKitProvider.currentEstablishedCall()
            if call != nil {
                var top = self.window?.rootViewController
                while (top?.presentedViewController != nil) {
                    top = top?.presentedViewController
                }
                if !(top! is CallVC) {
                    let callVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallVC") as! CallVC
                    callVC.call = call
                    top?.present(callVC, animated: true, completion: nil)
                }
            }
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        var top = self.window?.rootViewController
        while top?.presentedViewController != nil {
            top = top?.presentedViewController
        }
        if top! is UITabBarController {
            setBadges(controller: top as! UITabBarController)
        }
        if User.currentUser() != nil {
            updateCurrentUserInFirestore(withValues: [ISONLINE : true]) { (success) in
                
            }
        }
        locationManagerStart()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        self.push.application(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let firebaseAuth = Auth.auth()
        if firebaseAuth.canHandleNotification(userInfo) {
            return
        } else {
//            self.push.application(application, didReceiveRemoteNotification: userInfo)
        }
    }
    
    func makeTransitionToMain() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: USER_DID_LOGIN_NOTIFICATION), object: nil, userInfo: [USERID: User.currentId()])
        if let controller = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainVC") as? UITabBarController{
            self.window?.rootViewController = controller
        }
    }

    func locationManagerStart() {
        if locationManager == nil {
            locationManager = CLLocationManager()
            locationManager!.delegate = self
            locationManager!.desiredAccuracy = kCLLocationAccuracyBest
            locationManager!.requestWhenInUseAuthorization()
        }
        locationManager!.startUpdatingLocation()
    }
    
    func locationMangerStop() {
        if locationManager != nil {
            locationManager!.stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("faild to get location")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
        case .authorizedWhenInUse:
            manager.startUpdatingLocation()
        case .authorizedAlways:
            manager.startUpdatingLocation()
        case .restricted:
            print("restricted")
        case .denied:
            locationManager = nil
            print("denied location access")
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        coordinates = locations.last!.coordinate
    }
    
    func startOneSignal() {
        let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
        let userID = status.subscriptionStatus.userId
        let pushToken = status.subscriptionStatus.pushToken
        if pushToken != nil {
            if let playerID = userID {
                UserDefaults.standard.set(playerID, forKey: PUSHID)
            } else {
                UserDefaults.standard.removeObject(forKey: PUSHID)
            }
            UserDefaults.standard.synchronize()
        }
        updateOneSignalId()
    }
    
    func initSinchWithUserId(userId: String) {
        if client == nil {
            client = Sinch.client(withApplicationKey: SINCHKEY, applicationSecret: SINCHSECRET, environmentHost: "sandbox.sinch.com", userId: userId)
            client.delegate = self
            client.call()?.delegate = self
            client.setSupportCalling(true)
            client.enableManagedPushNotifications()
            client.start()
            client.startListeningOnActiveConnection()
            
            callKitProvider = SINCallKitProvider(withClient: client)
        }
    }

    func managedPush(_ managedPush: SINManagedPush!, didReceiveIncomingPushWithPayload payload: [AnyHashable : Any]!, forType pushType: String!) {
        let result = SINPushHelper.queryPushNotificationPayload(payload)
        if result!.isCall() {
            print("incoming push payload")
            self.handleRemoteNotification(userInfo: payload as NSDictionary)
        }
    }

    func handleRemoteNotification(userInfo: NSDictionary) {
        if client == nil {
            let userId = UserDefaults.standard.object(forKey: USERID)
            if userId != nil {
                self.initSinchWithUserId(userId: userId as! String)
            }
        }

        let result = self.client.relayRemotePushNotification(userInfo as! [AnyHashable : Any])
        if result!.isCall() {
            print("handle call notification")
        }
        if result!.isCall() && result!.call()!.isCallCanceled {
            self.presentMissedCallNotificationWithRemoteUserId(userId: result!.call()!.callId)
        }
    }
    
    func presentMissedCallNotificationWithRemoteUserId(userId: String) {
        if UIApplication.shared.applicationState == .background {
            let center = UNUserNotificationCenter.current()
            let content = UNMutableNotificationContent()
            content.title = "Missed Call"
            content.body = "From \(userId)"
            content.sound = UNNotificationSound.default
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
            let request = UNNotificationRequest(identifier: "ContentIdentifier", content: content, trigger: trigger)
            center.add(request) { (error) in
                if error != nil {
                    print("error on notification", error!.localizedDescription)
                }
            }
        }
    }

    func client(_ client: SINCallClient!, willReceiveIncomingCall call: SINCall!) {
        print("will receive incoming call")
        callKitProvider.reportNewIncomingCall(call: call)
    }

    func client(_ client: SINCallClient!, didReceiveIncomingCall call: SINCall!) {
        print("did receive call")
        var root = self.window?.rootViewController
        while (root?.presentedViewController != nil) {
            root = root?.presentedViewController
        }
        let callVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CallVC") as! CallVC
        callVC.call = call
        root?.present(callVC, animated: true, completion: nil)
    }

    func clientDidStart(_ client: SINClient!) {
        print("Sinch did start")
    }

    func clientDidStop(_ client: SINClient!) {
        print("Sinch did stop")
    }

    func clientDidFail(_ client: SINClient!, error: Error!) {
        print("Sinch did fail \(error.localizedDescription)")
    }

    func voipRegistration() {
        let voipRegistry: PKPushRegistry = PKPushRegistry(queue: DispatchQueue.main)
        voipRegistry.delegate = self
        voipRegistry.desiredPushTypes = [PKPushType.voIP]
    }

    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {

    }

    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType) {
        print("did get incoming push")
        self.handleRemoteNotification(userInfo: payload.dictionaryPayload as NSDictionary)
    }
}

