//
//  AppButtonWithoutFrame.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/6/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit

class AppButtonWithoutFrame: UIButton {
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        initView()
    }
    
    override func awakeFromNib() {
        initView()
    }
    
    func initView() {
        let title = self.title(for: .normal)?.localized()
        setTitle(title, for: .normal)
        titleLabel?.font = UIFont(name: "Avenir-Light", size: 22)
        titleLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }

}
