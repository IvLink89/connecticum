//
//  AppMediumLabel.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/2/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit

@IBDesignable
class AppMediumLabel: UILabel {

    @IBInspectable var fontSize: CGFloat = 26 {
        didSet{
            initView()
        }
    }
    
    override func prepareForInterfaceBuilder() {
        initView()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }
    
    func initView() {
        text = text?.localized()
        textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        font = UIFont(name: "Avenir-Medium", size: fontSize)
    }
}
