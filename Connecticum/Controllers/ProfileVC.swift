//
//  ProfileVC.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/9/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit
import ProgressHUD
import Sinch

class ProfileVC: UIViewController {

    @IBOutlet weak var ivAvatar: UIImageView!
    @IBOutlet weak var lblName: AppMediumLabel!
    @IBOutlet weak var lblPhone: AppMediumLabel!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var btnBlockUser: AppButtonWithoutFrame!
    
    var user:User?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "profile_title".localized()
//        navigationController?.navigationBar.backItem?.title = ""
        setupData()
        updateBlockStatus()
    }
    
    @IBAction func callClicked(_ sender: Any) {
        callUser()
        let currUser = User.currentUser()
        let call = Call(callerId: (currUser?.objectId)!, opponentId: (user?.objectId)!, callerName: (currUser?.fullname)!, opponentName: (user?.fullname)!)
        call.saveCallInBackground()
    }
    
    @IBAction func messageClicked(_ sender: Any) {
        if !checkBlockedStatus(withUser: user!) {
            let chatVC = ChatConversationVC()
            chatVC.titleName = user!.firstname
            chatVC.membersToPush = [User.currentId(), user!.objectId]
            chatVC.membersIds = [User.currentId(), user!.objectId]
            chatVC.chatRoomId = startPrivateChat(owner: User.currentUser()!, user: user!)
            chatVC.isGroup = false
            chatVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(chatVC, animated: true)
        } else {
            ProgressHUD.showError("users_blocked_user_alert".localized())
        }
    }

    @IBAction func blockUserClicked(_ sender: Any) {
        var blockedIds = User.currentUser()?.blockedUsers
        if (blockedIds?.contains(user?.objectId ?? "none"))!{
            let index = blockedIds?.firstIndex(of: user?.objectId ?? "none")
            blockedIds?.remove(at: index!)
        }else{
            blockedIds?.append(user?.objectId ?? "none")
        }
        updateCurrentUserInFirestore(withValues: [BLOCKEDUSERID : blockedIds!]) { (error) in
            if error != nil{
                return
            }
            self.updateBlockStatus()
        }
        blockUser(userToBlock: user!)
    }
    
    
    func setupData() {
        if user != nil{
            lblName.text = user?.fullname
            lblPhone.text = user?.phoneNumber
            if user?.avatar != ""{
                imageFromData(pictureData: user?.avatar ?? "") { (image) in
                    if image != nil{
                        ivAvatar.image = image?.circleMasked
                    }
                }
            }
        }
    }
    
    func updateBlockStatus()  {
        if user?.objectId != User.currentId(){
            btnBlockUser.isHidden = false
            btnCall.isHidden = false
            btnMessage.isHidden = false
        }else{
            btnBlockUser.isHidden = true
            btnCall.isHidden = true
            btnMessage.isHidden = true
        }
        if (User.currentUser()?.blockedUsers.contains(user?.objectId ?? ""))!{
            btnBlockUser.setTitle("profile_unblock_btn".localized(), for: .normal)
        }else{
            btnBlockUser.setTitle("profile_block_btn".localized(), for: .normal)
        }
    }
    
    func getCallClient() -> SINCallClient {
        return appDelegate.client.call()
    }
    
    func callUser() {
        let userId = user?.objectId
        let call = getCallClient().callUser(withId: userId)
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "CallVC") as? CallVC {
            controller.call = call
            self.presentDetail(controller)
        }
    }
}
