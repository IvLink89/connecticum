//
//  GroupsVC.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/20/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit
import ProgressHUD
import ImagePicker

class GroupVC: UIViewController, ImagePickerDelegate {
    
    @IBOutlet weak var ivCamera: UIImageView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var tfGroupName: UITextField!
    
    var group: NSDictionary!
    var groupIcon: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ivCamera.isUserInteractionEnabled = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(cameraIconTapped))
        tap.cancelsTouchesInView = false
        ivCamera.addGestureRecognizer(tap)
        
        setupUI()
        
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(image: UIImage(named: "invite"), style: .plain, target: self, action: #selector(self.inviteUsers)), UIBarButtonItem(image: UIImage(named: "save"), style: .plain, target: self, action: #selector(self.saveButtonPressed))]
    }
    
    @IBAction func editButtonPressed(_ sender: Any) {
        showIconOptions()
    }
    
    @objc func cameraIconTapped(_ sender: Any) {
        showIconOptions()
    }
    
    @objc func saveButtonPressed(_ sender: Any) {
        var withValues : [String : Any]!
        if tfGroupName.text != "" {
            withValues = [NAME : tfGroupName.text!]
        } else {
            ProgressHUD.showError("Subject is required!")
            return
        }
        
        let avatarString = getBase64FromImage(image: ivCamera.image!)
        
        withValues = [NAME : tfGroupName.text!, AVATAR : avatarString!]
        Group.updateGroup(groupId: group[GROUPID] as! String, withValues: withValues)
        withValues = [WITHUSERNAME : tfGroupName.text!, AVATAR : avatarString ?? ""]
        updateExistingChatWithNewValues(chatRoomId: group[GROUPID] as! String, members: group[MEMBERS] as! [String], withValues: withValues)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func inviteUsers() {
        let userVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InviteVC") as! InviteVC
        userVC.group = group
        self.navigationController?.pushViewController(userVC, animated: true)
    }

    func setupUI() {
        self.title = "Group"
        tfGroupName.text = group[NAME] as? String
        imageFromData(pictureData: group[AVATAR] as! String) { (avatarImage) in
            if avatarImage != nil {
                self.ivCamera.image = avatarImage!.circleMasked
            }
        }
    }
    
    func showIconOptions() {
        let optionMenu = UIAlertController(title: "Choose group Icon", message: nil, preferredStyle: .actionSheet)
        let takePhotoAction = UIAlertAction(title: "Take/Choose Photo", style: .default) { (alert) in
            let imagePickerController = ImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.imageLimit = 1
            self.present(imagePickerController, animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert) in
            
        }
        
        if groupIcon != nil {
            let resetAction = UIAlertAction(title: "Reset", style: .default) { (alert) in
                self.groupIcon = nil
                self.ivCamera.image = UIImage(named: "cameraIcon")
                self.btnEdit.isHidden = true
            }
            optionMenu.addAction(resetAction)
        }
        optionMenu.addAction(takePhotoAction)
        optionMenu.addAction(cancelAction)
        
        if ( UI_USER_INTERFACE_IDIOM() == .pad ){
            if let currentPopoverpresentioncontroller = optionMenu.popoverPresentationController{
                currentPopoverpresentioncontroller.sourceView = btnEdit
                currentPopoverpresentioncontroller.sourceRect = btnEdit.bounds
                currentPopoverpresentioncontroller.permittedArrowDirections = .up
                self.present(optionMenu, animated: true, completion: nil)
            }
        } else {
            self.present(optionMenu, animated: true, completion: nil)
        }
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        self.dismiss(animated: true, completion: nil)
    }

    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        if images.count > 0 {
            self.groupIcon = images.first!
            self.ivCamera.image = self.groupIcon?.circleMasked
            self.btnEdit.isHidden = false
        }
        self.dismiss(animated: true, completion: nil)
    }

    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }

}
