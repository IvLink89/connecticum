//
//  ChatsVC.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/9/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit
import Firebase
import ProgressHUD

class ChatsVC: UIViewController, UserCellDelegate, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {

    @IBOutlet weak var tableViewChats: UITableView!
    var chats: [NSDictionary] = []
    var filteredChats: [NSDictionary] = []
    var chatListener: ListenerRegistration!
    var searchController = UISearchController(searchResultsController: nil)

    override func viewWillAppear(_ animated: Bool) {
        loadChats()
    }

    override func viewWillDisappear(_ animated: Bool) {
        chatListener.remove()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "chats_title".localized()
        //        navigationItem.searchController = searchController #TODO setup search, previuosly didn't work cotroller, cause din't hide while srcoll
//        searchController.searchResultsUpdater = self
//        searchController.dimsBackgroundDuringPresentation = false
//        searchController.hidesNavigationBarDuringPresentation = false
//        definesPresentationContext = true
//        searchController.searchBar.tintColor = #colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1)
//        navigationItem.hidesSearchBarWhenScrolling = true

        tableViewChats.tableFooterView = UIView()
//        tabBarController?.tabBar.backgroundImage = UIImage()
//        tabBarController?.tabBar.shadowImage = UIImage()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("numberOfRowsInSection CHAT \(chats)")
        print("numberOfRowsInSection \(chats.count)")
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredChats.count
        }
        return chats.count
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableViewChats.deselectRow(at: indexPath, animated: true)
        var tempDict: NSDictionary!
        if searchController.isActive && searchController.searchBar.text != "" {
            if chats.count>0{
                tempDict = filteredChats[indexPath.row]
            }else{
                tempDict = NSDictionary()
            }
        } else {
            if chats.count>0{
                tempDict = chats[indexPath.row]
            }else{
                tempDict = NSDictionary()
            }
        }
        restartChat(chat: tempDict)
        
        let chatConversationVC = ChatConversationVC()
        chatConversationVC.hidesBottomBarWhenPushed = true
        chatConversationVC.membersIds = tempDict[MEMBERS] as? [String]
        chatConversationVC.membersToPush = tempDict[MEMBERSTOPUSH] as? [String]
        chatConversationVC.titleName = tempDict[WITHUSERNAME] as? String
        chatConversationVC.chatRoomId = tempDict[CHATROOMID] as? String
        chatConversationVC.isGroup = (tempDict[TYPE] as? String) == GROUP
        navigationController?.pushViewController(chatConversationVC, animated: true)
    }

    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        var tempDict: NSDictionary!
        if searchController.isActive && searchController.searchBar.text != "" {
            tempDict = filteredChats[indexPath.row]
        } else {
            tempDict = chats[indexPath.row]
        }

        var muteTitle = "chat_unmute_item".localized()
        var muted = false
        var imageMute = UIImage(named: "speaker-mute")

        if (tempDict[MEMBERSTOPUSH] as! [String]).contains(User.currentId()) {
            muteTitle = "chat_mute_item".localized()
            muted = true
            imageMute = UIImage(named: "speaker-up")
        }
        let actionRemove = UIContextualAction(style: .destructive, title: "chat_remove_item".localized()) { (action, view, handler) in
            print("actionRemove")
            removeChat(chatDict: tempDict)
            self.chats.remove(at: indexPath.row)
            self.tableViewChats.reloadData()
        }
        let actionMute = UIContextualAction(style: .destructive, title: muteTitle) { (action, view, handler) in
            self.updatePushMembers(chat: tempDict, mute: muted)
            print("actionMute")
        }
        actionRemove.image = UIImage(named: "delete-bin")
        actionRemove.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.262745098, blue: 0.2117647059, alpha: 1)
        actionMute.image = imageMute
        actionMute.backgroundColor = #colorLiteral(red: 0, green: 0.5988745093, blue: 0.9821230769, alpha: 1)
        let config = UISwipeActionsConfiguration(actions: [actionRemove, actionMute])
        return config
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("cellForRowAt \(indexPath)")
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath) as? ChatCell {
            var chat: NSDictionary?
            if searchController.isActive && searchController.searchBar.text != "" {
                if filteredChats.count > 0{
                    chat = filteredChats[indexPath.row]
                }
            } else {
                if chats.count > 0{
                    chat = chats[indexPath.row]
                }
            }
            cell.generateUserCell(chat: chat ?? NSDictionary(), indexPath: indexPath)
            cell.userCellDelegate = self
            return cell
        }
        return UITableViewCell()
    }

    @IBAction func createChatClicked(_ sender: Any) {
        selectUserForChat(isGroup: false)
    }

    @IBAction func createGroupClicked(_ sender: Any) {
        selectUserForChat(isGroup: true)
    }

    func selectUserForChat(isGroup: Bool) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactsVC") as? ContactsVC {
            controller.isGroup = isGroup
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func didClickAvatar(indexPath: IndexPath) {
        let chat: NSDictionary!
        if searchController.isActive && searchController.searchBar.text != "" {
            chat = filteredChats[indexPath.row]
        } else {
            chat = chats[indexPath.row]
        }
        if chat[TYPE] as! String == PRIVATE {
            reference(.User).document(chat[WITHUSERID] as! String).getDocument { (snapshot, error) in
                guard let snapshot = snapshot else {
                    return
                }
                if snapshot.exists {
                    let userDict = snapshot.data()! as NSDictionary
                    let tempUser = User(_dictionary: userDict)
                    if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as? ProfileVC {
                        controller.user = tempUser
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                }
            }
        }
    }

    func loadChats() {
        ProgressHUD.show()
        chatListener = reference(.Chat).whereField(USERID, isEqualTo: User.currentId()).addSnapshotListener({ (snapshot, error) in
            if error != nil {
                self.tableViewChats.reloadData()
                print(error?.localizedDescription ?? "None")
                ProgressHUD.showError(error?.localizedDescription)
                return
            }
            guard let snapshot = snapshot else {
                return
            }
            self.chats = []
            if !snapshot.isEmpty {
                let sortedDict = (dictionaryFromSnapshots(snapshots: snapshot.documents) as NSArray)
                        .sortedArray(using: [NSSortDescriptor(key: DATE, ascending: false)]) as! [NSDictionary]
                for chat in sortedDict {
                    if chat[LASTMESSAGE] as! String != "" && chat[CHATROOMID] != nil && chat[CHAT_ID] != nil {
                        self.chats.append(chat)
                    }
                }
                self.tableViewChats.reloadData()
            }
        })
        ProgressHUD.dismiss()
    }

    func updateSearchResults(for searchController: UISearchController) {
        filterContent(searchText: searchController.searchBar.text!)
    }

    func filterContent(searchText: String, scope: String = "All") {
        filteredChats = chats.filter({ (chat) -> Bool in
            return (chat[WITHUSERNAME] as! String).lowercased().contains(searchText.lowercased())
        })
        tableViewChats.reloadData()
    }
    
    func updatePushMembers(chat: NSDictionary, mute: Bool) {
        var membersToPush = chat[MEMBERSTOPUSH] as! [String]
        if mute {
            let index = membersToPush.firstIndex(of: User.currentId())!
            membersToPush.remove(at: index)
        } else {
            membersToPush.append(User.currentId())
        }
        updateExistingChatWithNewValues(chatRoomId: chat[CHATROOMID] as! String, members: chat[MEMBERS] as! [String], withValues: [MEMBERSTOPUSH : membersToPush])
    }
}
