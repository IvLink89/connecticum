//
//  PictureCell.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/19/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit

class PictureCell: UICollectionViewCell {
    
    @IBOutlet weak var ivPicture: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func generateCell(image: UIImage)  {
        ivPicture.image = image
    }
}
