//
//  AudioControllerDelegate.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/26/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import Foundation
import Sinch

class AudioControllerDelegate: NSObject, SINAudioControllerDelegate  {
    
    var muted: Bool!
//    var speaker: Bool!//not needed
    
    func audioControllerMuted(_ audioController: SINAudioController!) {
        self.muted = true
    }
    
    func audioControllerUnmuted(_ audioController: SINAudioController) {
        self.muted = false
    }
    
//    //not needed
//    func audioControllerSpeakerEnabled(_ audioController: SINAudioController!) {
//        self.speaker = true
//    }
//    
//    func audioControllerSpeakerDisabled(_ audioController: SINAudioController!) {
//        self.speaker = false
//    }
}
