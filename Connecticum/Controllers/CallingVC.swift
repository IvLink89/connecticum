//
//  CallingVC.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/20/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit
import ProgressHUD
import FirebaseFirestore

class CallingVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating {

    var allCalls: [Call] = []
    var filteredCalls: [Call] = []
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    let searchController = UISearchController(searchResultsController: nil)
    var callListener: ListenerRegistration!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        loadCalls()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        callListener.remove()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBadges(controller: self.tabBarController!)

        tableView.tableFooterView = UIView()
        
//        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = true
        navigationItem.title = "Calls"
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        
        tableView.sectionIndexColor = #colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1)
        searchController.searchBar.tintColor = #colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredCalls.count
        }
        return allCalls.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CallCell", for: indexPath) as! CallCell
        var call: Call!
        if searchController.isActive && searchController.searchBar.text != "" {
            call = filteredCalls[indexPath.row]
        } else {
            call = allCalls[indexPath.row]
        }
        cell.generateCell(call: call)
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            var tempCall: Call!
            if searchController.isActive && searchController.searchBar.text != "" {
                tempCall = filteredCalls[indexPath.row]
                filteredCalls.remove(at: indexPath.row)
            } else {
                tempCall = allCalls[indexPath.row]
                allCalls.remove(at: indexPath.row)
            }
            tempCall.deleteCall()
            tableView.reloadData()
        }
    }
    
    func loadCalls() {
        callListener = reference(.Call).document(User.currentId()).collection(User.currentId()).order(by: DATE, descending: true).limit(to: 20).addSnapshotListener({ (snapshot, error) in
            
            self.allCalls = []
            guard let snapshot = snapshot else { return }
            if !snapshot.isEmpty {
                let sortedDictionary = dictionaryFromSnapshots(snapshots: snapshot.documents)
                for callDictionary in sortedDictionary {
                    let call = Call(dict: callDictionary)
                    self.allCalls.append(call)
                }
            }
            self.tableView.reloadData()
        })
    }
    
    func filteredContentForSearchText(searchText: String, scope: String = "All") {
        filteredCalls = allCalls.filter({ (call) -> Bool in
            var callerName: String!
            if call.callerId == User.currentId() {
                callerName = call.opponentFullName
            } else {
                callerName = call.callerFullName
            }
            return (callerName).lowercased().contains(searchText.lowercased())
        })
        tableView.reloadData()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredContentForSearchText(searchText: searchController.searchBar.text!)
    }
}
