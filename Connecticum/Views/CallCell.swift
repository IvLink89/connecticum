//
//  CallCell.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/22/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit

class CallCell: UITableViewCell {

    @IBOutlet weak var lblName: AppMediumLabel!
    @IBOutlet weak var lblStatus: AppMediumLabel!
    @IBOutlet weak var lblDate: AppMediumLabel!
    @IBOutlet weak var ivAvatar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func generateCell(call: Call) {
        lblDate.text = formatCallTime(date: call.callDate!)
        lblStatus.text = ""
        if call.callerId == User.currentId(){
            lblStatus.text = "call_outgoing_call".localized()
            lblName.text = call.opponentFullName
            ivAvatar.image = UIImage(named: "phone-outgoing")
        }else{
            lblStatus.text = "call_incoming_call".localized()
            lblName.text = call.callerFullName
            ivAvatar.image = UIImage(named: "phone-incoming")
        }
    }

}
