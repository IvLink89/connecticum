//
//  EditProfileVC.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/20/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit
import ProgressHUD
import ImagePicker

class EditProfileVC: UIViewController, ImagePickerDelegate{

    @IBOutlet weak var saveButtonOutlet: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var ivAvatar: UIImageView!
    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    
    var avatarImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    @IBAction func onBackClicked(_ sender: Any) {
        dismissDetail()
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        if tfFirstName.text != "" && tfLastName.text != "" && tfEmail.text != "" {
            ProgressHUD.show("Saving...")
            saveButtonOutlet.isEnabled = false
            let fullName = tfFirstName.text! + " " + tfLastName.text!
            var withValues = [FIRSTNAME : tfFirstName.text!, LASTNAME : tfLastName.text!, FULLNAME : fullName]
            
            if avatarImage != nil {
                withValues[AVATAR] = getBase64FromImage(image: avatarImage!)
            }
            
            updateCurrentUserInFirestore(withValues: withValues) { (error) in
                if error != nil {
                    DispatchQueue.main.async {
                        ProgressHUD.showError(error!.localizedDescription)
                        print("couldn update user \(error!.localizedDescription)")
                    }
                    self.saveButtonOutlet.isEnabled = true
                    return
                }
                ProgressHUD.showSuccess("Saved")
                self.saveButtonOutlet.isEnabled = true
                self.navigationController?.popViewController(animated: true)
            }
        } else {
            ProgressHUD.showError("All fields are required!")
        }
    }
    
    @objc func avatarTap(_ sender: Any) {
        let imagePicker = ImagePickerController()
        imagePicker.delegate = self
        imagePicker.imageLimit = 1
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func setupUI() {
        saveButtonOutlet.imageView?.image = saveButtonOutlet.imageView?.image?.imageWithColor(#colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1))
        btnBack.imageView?.image = btnBack.imageView?.image?.imageWithColor(#colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1))
        
        let currentUser = User.currentUser()!
        ivAvatar.isUserInteractionEnabled = true
        tfFirstName.text = currentUser.firstname
        tfLastName.text = currentUser.lastname
        tfEmail.text = currentUser.email
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(avatarTap))
        tap.cancelsTouchesInView = false
        ivAvatar.addGestureRecognizer(tap)
        
        if currentUser.avatar != "" {
            imageFromData(pictureData: currentUser.avatar) { (avatarImage) in
                if avatarImage != nil {
                    self.ivAvatar.image = avatarImage!.circleMasked
                }
            }
        }
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        self.dismiss(animated: true, completion: nil)
    }

    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {

        if images.count > 0 {
            self.avatarImage = images.first!
            self.ivAvatar.image = self.avatarImage!.circleMasked
        }

        self.dismiss(animated: true, completion: nil)
    }

    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }

}
