//
//  NewGroupVC.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/20/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit
import ProgressHUD
import ImagePicker

class NewGroupVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, GroupUsersDelegate, ImagePickerDelegate {

    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var ivAvatar: UIImageView!
    @IBOutlet weak var tfSubject: UITextField!
    @IBOutlet weak var participantsLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var memberIds: [String] = []
    var allMembers: [User] = []
    var groupIcon: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ivAvatar.isUserInteractionEnabled = true
        collectionView.delegate = self
        collectionView.dataSource = self
        navigationItem.leftBarButtonItem?.title = " "
        navigationItem.backBarButtonItem?.title = " "
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(groupIconTapped))
        tap.cancelsTouchesInView = false
        ivAvatar.addGestureRecognizer(tap)
        
        updateParticipantsLabel()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allMembers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GroupUserCell", for: indexPath) as! GroupUserCell
        cell.delegate = self
        cell.generateCell(user: allMembers[indexPath.row], indexPath: indexPath)
        return cell
    }
    
    @objc func createButtonPressed(_ sender: Any) {
        if tfSubject.text != "" {
            memberIds.append(User.currentId())
            var avatar = getBase64FromImage(image: UIImage(named: "groupIcon")!)
            
            if groupIcon != nil {
                avatar = getBase64FromImage(image: groupIcon!)
            }
            let groupId = UUID().uuidString
            let group = Group(groupId: groupId, subject: tfSubject.text!, ownerId: User.currentId(), members: memberIds, avatar: avatar!)
            
            group.saveGroup()
            startGroupChat(group: group)
            
            let chatVC = ChatConversationVC()
            chatVC.titleName = group.groupDictionary[NAME] as? String
            chatVC.membersIds = group.groupDictionary[MEMBERS] as? [String]
            chatVC.membersToPush = group.groupDictionary[MEMBERS] as? [String]
            chatVC.chatRoomId = groupId
            chatVC.isGroup = true
            chatVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(chatVC, animated: true)
        } else {
            ProgressHUD.showError("Subject is required!")
        }
    }
    
    @objc func groupIconTapped(_ sender: Any) {
        showIconOptions()
    }
    
    @IBAction func onEditButtonClicked(_ sender: Any) {
        showIconOptions()
    }
    
    func didClickDelete(indexPath: IndexPath) {
        allMembers.remove(at: indexPath.row)
        memberIds.remove(at: indexPath.row)
        collectionView.reloadData()
        updateParticipantsLabel()
    }
    
    func showIconOptions() {
        let optionMenu = UIAlertController(title: "Choose group Icon", message: nil, preferredStyle: .actionSheet)
        let takePhotoAction = UIAlertAction(title: "Take/Choose Photo", style: .default) { (alert) in
            let imagePicker = ImagePickerController()
            imagePicker.delegate = self
            imagePicker.imageLimit = 1
            self.present(imagePicker, animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert) in
            
        }
        
        if groupIcon != nil {
            let resetAction = UIAlertAction(title: "Reset", style: .default) { (alert) in
                self.groupIcon = nil
                self.ivAvatar.image = UIImage(named: "cameraIcon")
                self.btnEdit.isHidden = true
            }
            optionMenu.addAction(resetAction)
        }
        optionMenu.addAction(takePhotoAction)
        optionMenu.addAction(cancelAction)
        
        if ( UI_USER_INTERFACE_IDIOM() == .pad ){
            if let currentPopoverpresentioncontroller = optionMenu.popoverPresentationController{
                currentPopoverpresentioncontroller.sourceView = btnEdit
                currentPopoverpresentioncontroller.sourceRect = btnEdit.bounds
                currentPopoverpresentioncontroller.permittedArrowDirections = .up
                self.present(optionMenu, animated: true, completion: nil)
            }
        } else {
            self.present(optionMenu, animated: true, completion: nil)
        }
    }
    
    func updateParticipantsLabel() {
        participantsLabel.text = "PARTICIPANTS: \(allMembers.count)"
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(title: "Create", style: .plain, target: self, action: #selector(self.createButtonPressed))]
        self.navigationItem.rightBarButtonItem?.isEnabled = allMembers.count > 0
    }

    

    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        self.dismiss(animated: true, completion: nil)
    }

    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        if images.count > 0 {
            self.groupIcon = images.first!
            self.ivAvatar.image = self.groupIcon!.circleMasked
            self.btnEdit.isHidden = false
        }
        self.dismiss(animated: true, completion: nil)
    }

    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}
