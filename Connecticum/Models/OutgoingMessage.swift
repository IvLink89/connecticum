//
//  MessagesHelper.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/16/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import Foundation

class OutgoingMessage {
    
    let messageDict: NSMutableDictionary

    init(message: String, senderId: String, senderName: String, date: Date, status: String, type: String) {
        messageDict = NSMutableDictionary(objects: [message, senderId, senderName, dateFormatter().string(from: date), status, type], forKeys: [MESSAGE as NSCopying, SENDERID as NSCopying, SENDERNAME as NSCopying, DATE as NSCopying, STATUS as NSCopying, TYPE as NSCopying])
    }
    
    init(message: String, pictureLink: String, senderId: String, senderName: String, date: Date, status: String, type: String) {
        messageDict = NSMutableDictionary(objects: [message, pictureLink, senderId, senderName, dateFormatter().string(from: date), status, type], forKeys: [MESSAGE as NSCopying, PICTURE as NSCopying, SENDERID as NSCopying, SENDERNAME as NSCopying, DATE as NSCopying, STATUS as NSCopying, TYPE as NSCopying])
    }
    
    init(message: String, audio: String, senderId: String, senderName: String, date: Date, status: String, type: String) {
        messageDict = NSMutableDictionary(objects: [message, audio, senderId, senderName, dateFormatter().string(from: date), status, type], forKeys: [MESSAGE as NSCopying, AUDIO as NSCopying, SENDERID as NSCopying, SENDERNAME as NSCopying, DATE as NSCopying, STATUS as NSCopying, TYPE as NSCopying])
    }
    
    init(message: String, video: String, thumbNail: NSData, senderId: String, senderName: String, date: Date, status: String, type: String) {
        let picThumb = thumbNail.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        messageDict = NSMutableDictionary(objects: [message, video, picThumb, senderId, senderName, dateFormatter().string(from: date), status, type], forKeys: [MESSAGE as NSCopying, VIDEO as NSCopying, PICTURE as NSCopying, SENDERID as NSCopying, SENDERNAME as NSCopying, DATE as NSCopying, STATUS as NSCopying, TYPE as NSCopying])
    }
    
    init(message: String, latitude: NSNumber, longitude: NSNumber, senderId: String, senderName: String, date: Date, status: String, type: String) {
        messageDict = NSMutableDictionary(objects: [message, latitude, longitude, senderId, senderName, dateFormatter().string(from: date), status, type], forKeys: [MESSAGE as NSCopying, LATITUDE as NSCopying, LONGITUDE as NSCopying, SENDERID as NSCopying, SENDERNAME as NSCopying, DATE as NSCopying, STATUS as NSCopying, TYPE as NSCopying])
    }
    
    
    func sendMessage(chatRoomID: String, messageDict: NSMutableDictionary, memberIds: [String], membersToPush: [String]) {
        let messageId = UUID().uuidString
        messageDict[MESSAGEID] = messageId
        
        for memberId in memberIds {
            reference(.Message).document(memberId).collection(chatRoomID).document(messageId).setData(messageDict as! [String : Any])
        }
        
        updateChats(chatRoomId: chatRoomID, lastMessage: messageDict[MESSAGE] as! String)
        let pushText = "[\(messageDict[TYPE] as! String) message]"
        sendPush(membersToPush: membersToPush, msg: pushText)
    }
    
    class func deleteMessage(withId: String, chatRoomId: String) {
        reference(.Message).document(User.currentId()).collection(chatRoomId).document(withId).delete()
    }
    
    class func updateMessage(withId: String, chatRoomId: String, memberIds: [String]) {
        let readDate = dateFormatter().string(from: Date())
        let values = [STATUS : READ, READDATE : readDate]
        
        for userId in memberIds {
            reference(.Message).document(userId).collection(chatRoomId).document(withId).getDocument { (snapshot, error) in
                guard let snapshot = snapshot  else { return }
                if snapshot.exists {
                    reference(.Message).document(userId).collection(chatRoomId).document(withId).updateData(values)
                }
            }
        }
    }
}
