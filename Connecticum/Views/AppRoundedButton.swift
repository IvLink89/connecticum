//
//  AppRoundedButton.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/2/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit

class AppRoundedButton: UIButton {

    @IBInspectable var cornerRadius: CGFloat = 5 {
        didSet{
            initView()
        }
    }
    
    @IBInspectable var rounded: Bool = false {
        didSet {
            initView()
        }
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        initView()
    }
    
    override func awakeFromNib() {
        initView()
    }
    
    func initView() {
        layer.cornerRadius = rounded ? frame.size.height/1.5 : cornerRadius
        let title = self.title(for: .normal)?.localized()
        setTitle(title, for: .normal)
//        titleLabel?.text = titleLabel?.text?.localized()
        titleLabel?.font = UIFont(name: "Avenir-Light", size: 22)
        titleLabel?.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }

}
