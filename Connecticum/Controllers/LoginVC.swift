//
//  ViewController.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/2/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import UIKit
import os.log
import ProgressHUD

class LoginVC: UIViewController {

    @IBOutlet weak var btnRegister: AppRoundedButton!
    @IBOutlet weak var btnLogin: AppRoundedButton!
    @IBOutlet weak var tfEmail: AppTextField!
    @IBOutlet weak var tfPassword: AppTextField!
    @IBOutlet weak var ivBackground: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }

    @IBAction func onLoginClicked(_ sender: Any) {
        if tfEmail.text == ""{
            ProgressHUD.showError("login_field_email_empty".localized())
        }else if tfPassword.text == ""{
            ProgressHUD.showError("login_field_pass_empty".localized())
        } else{
            loginUser()
        }
    }
    
    @IBAction func onRegisterClicked(_ sender: Any) {
        registerUser()
    }
    
    func loginUser() {
        ProgressHUD.show("login_loading_message".localized())
        User.loginUserWith(email: tfEmail.text!, password: tfPassword.text!) { (error) in
            if error != nil {
                print(error?.localizedDescription ?? "none")
                ProgressHUD.showError(error?.localizedDescription)
                return
            }
            self.makeTransitionToMain()
        }
        clearTextFields()
    }
    
    func registerUser() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC"){
            self.presentDetail(controller)
        }
        clearTextFields()
    }
    
    func clearTextFields() {
        tfEmail.text = ""
        tfPassword.text = ""
    }
    
    func makeTransitionToMain() {
        ProgressHUD.dismiss()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: USER_DID_LOGIN_NOTIFICATION), object: nil, userInfo: [USERID: User.currentId()])
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "MainVC"){
            self.presentDetail(controller)
        }
        clearTextFields()
    }
}

