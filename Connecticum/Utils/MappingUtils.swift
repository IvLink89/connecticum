//
//  MappingUtils.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/15/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import Foundation
import FirebaseFirestore

func dictionaryFromSnapshots(snapshots: [DocumentSnapshot]) -> [NSDictionary] {
    var allMessages: [NSDictionary] = []
    for snapshot in snapshots {
        allMessages.append(snapshot.data()! as NSDictionary)
    }
    return allMessages
}
