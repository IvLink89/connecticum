//
//  PushNotifications.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/22/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import Foundation
import OneSignal

func sendPush(membersToPush:[String], msg: String)  {
    let updatedMembers = removeCurrUserFromPushes(membersToPush: membersToPush)
    getMembersToPushes(members: updatedMembers) { (usersPushIds) in
        let currUser = User.currentUser()!
        OneSignal.postNotification(["contents":["en":"\(currUser.firstname)\n\(msg)"], "ios_badge_type":"Increase", "ios_badge_count":"1","iclude_users_ids":usersPushIds ])
    }
}

func removeCurrUserFromPushes(membersToPush:[String]) ->[String] {
    var updatedMembers : [String] = []
    for member in membersToPush{
        if member != User.currentId(){
            updatedMembers.append(member)
        }
    }
    return updatedMembers
}

func getMembersToPushes(members:[String], completion: @escaping(_ membersToPush: [String])->Void) {
    var pushIds : [String] = []
    var count = 0
    for member in members{
        reference(.User).document(member).getDocument { (snapshot, error) in
            guard let snapshot = snapshot else {
                completion(pushIds)
                return
            }
            if snapshot.exists{
                let userDict = snapshot.data() as NSDictionary?
                let user = User(_dictionary: userDict!)
                pushIds.append(user.pushId!)
                count+=1
                if members.count == count{
                    completion(pushIds)
                }
            }else{
                completion(pushIds)
            }
        }
    }
}
