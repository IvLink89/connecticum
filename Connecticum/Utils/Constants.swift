//
//  Constants.swift
//  Connecticum
//
//  Created by Ivan Linnyk on 12/5/18.
//  Copyright © 2018 IvLink. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseFirestore

public var chatBadgeHandler: ListenerRegistration?
let userDefaults = UserDefaults.standard

public let PHOTO_PATH_LIBRARY = "PictureMessages/"
public let VIDEO_PATH_LIBRARY = "VideoMessages/"
public let AUDIO_PATH_LIBRARY = "AudioMessages/"

public let USER_DID_LOGIN_NOTIFICATION = "UserDidLoginNotification"
public let APP_STARTED_NOTIFICATION = "AppStartedNotification"

public let FILEREFERENCE = "gs://connecticum-5e16e.appspot.com/"
public let ONESIGNALAPPID = "598f6f08-aa5f-47e6-8d77-db92299ffbaf"
public let SINCHKEY = "b27d98bb-9f59-4f45-8bd0-14032f91f213"
public let SINCHSECRET = "8nGrTysQTk6VSQ651EpBcQ=="
public let APPURL = ""

public let USER_PATH = "User"
public let TYPINGPATH_PATH = "Typing"
public let RECENT_PATH = "Recent"
public let MESSAGE_PATH = "Message"
public let GROUP_PATH = "Group"
public let CALL_PATH = "Call"

public let OBJECTID = "objectId"
public let CREATEDAT = "createdAt"
public let UPDATEDAT = "updatedAt"
public let EMAIL = "email"
public let PHONE = "phone"
public let COUNTRYCODE = "countryCode"
public let FACEBOOK = "facebook"
public let LOGINMETHOD = "loginMethod"
public let PUSHID = "pushId"
public let FIRSTNAME = "firstname"
public let LASTNAME = "lastname"
public let FULLNAME = "fullname"
public let AVATAR = "avatar"
public let CURRENTUSER = "currentUser"
public let ISONLINE = "isOnline"
public let VERIFICATIONCODE = "firebase_verification"
public let CITY = "city"
public let COUNTRY = "country"
public let BLOCKEDUSERID = "blockedUserId"

public let BACKGROUBNDIMAGE = "backgroundImage"
public let SHOWAVATAR = "showAvatar"
public let PASSWORDPROTECT = "passwordProtect"
public let FIRSTRUN = "firstRun"
public let NUMBEROFMESSAGES = 10
public let MAXDURATION = 120.0
public let AUDIOMAXDURATION = 120.0
public let SUCCESS = 2
public let COMPRESSING_QUALITY = 0.3

public let CHATROOMID = "chatRoomID"
public let USERID = "userId"
public let DATE = "date"
public let PRIVATE = "private"
public let GROUP = "group"
public let GROUPID = "groupId"
public let CHAT_ID = "chatId"
public let MEMBERS = "members"
public let MESSAGE = "message"
public let MEMBERSTOPUSH = "membersToPush"
public let DESCRIPTION = "description"
public let LASTMESSAGE = "lastMessage"
public let COUNTER = "counter"
public let TYPE = "type"
public let WITHUSERNAME = "withUserName"
public let WITHUSERID = "withUserID"
public let OWNERID = "ownerID"
public let STATUS = "status"
public let MESSAGEID = "messageId"
public let NAME = "name"
public let SENDERID = "senderId"
public let SENDERNAME = "senderName"
public let THUMBNAIL = "thumbnail"
public let ISDELETED = "isDeleted"

public let CONTACT = "contact"
public let CONTACTID = "contactId"

public let PICTURE = "picture"
public let TEXT = "text"
public let VIDEO = "video"
public let AUDIO = "audio"
public let LOCATION = "location"

public let LATITUDE = "latitude"
public let LONGITUDE = "longitude"

public let DELIVERED = "delivered"
public let READ = "read"
public let READDATE = "readDate"
public let DELETED = "deleted"

public let DEVICEID = "deviceId"

public let ISINCOMING = "isIncoming"
public let CALLERID = "callerId"
public let CALLERFULLNAME = "callerFullName"
public let CALLSTATUS = "callStatus"
public let WITHUSERFULLNAME = "withUserFullName"
public let CALLERAVATAR = "callerAvatar"
public let WITHUSERAVATAR = "withUserAvatar"
